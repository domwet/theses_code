# Introduction

This repository contains the source code for my theses.
BA contains the Bachelorthesis which currently contains no code, but just the Thesis itself. MA contains the Masterthesis and the code of it.

- **Title of Bachelorthesis:** "Weiterentwicklung der Eclipse-Integration des GeneSEZ Generator Framework" (its obviously in German)
- **Title of Masterthesis:** "Enhanced depth estimation for 'freehand stereo' using PatchMatch Stereo" (this is in English)

## BA

As said before it just contains the Thesis itself, but I want to add the code as soon as I find it.
I did some enhancements of the GeneSEZ Generator Framework. See http://www.genesez.org for more information.

## MA

This contains the thesis itself and the source code for it. I licenced my part under the WTFPL (see: http://www.wtfpl.net"), but it does contain source under the GPL v3 and under the Apache Licence v2. Currently it should not be compilable because some things are missing (mainly because of licencing issues) see the  LICENCES file for more information.

I implemented the Paper "PatchMatch Stereo - Stereo Matching with Slanted Support Windows" by Michael Bleyer, Christoph Rhemann and Carsten Rother, 2011, BMVC (see http://research.microsoft.com/pubs/152957/PatchMatchStereo_BMVC2011_6MB.pdf) and did some modifications to it. See my Masterthesis for this.
