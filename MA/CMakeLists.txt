# Copyright (C) 2015 Dominik Wetzel <dominik.wetzel@fh-zwickau.de>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

cmake_minimum_required(VERSION 2.4)
if(COMMAND cmake_policy)
  cmake_policy(SET CMP0003 NEW)
endif(COMMAND cmake_policy)

project(patchmatchstereo)

IF(WIN32)
  set(CMAKE_FIND_LIBRARY_SUFFIXES .lib) #${CMAKE_STATIC_LIBRARY_SUFFIX})
  message("Library Suffixes: ${CMAKE_FIND_LIBRARY_SUFFIXES}") 
ENDIF()

include(FindOpenMP)
if(OPENMP_FOUND)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS} -O3 -ggdb -std=c++11 -s -ffast-math -fprofile-use -fomit-frame-pointer -fstrength-reduce -msse2")
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} ${OpenMP_CXX_FLAGS} -Wall -ggdb -std=c++11")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")
endif()

IF(WIN32)
  set(OPENCV_STATIC ON)
ENDIF()

find_package(OpenCV REQUIRED)
find_package(OpenCL REQUIRED)

add_subdirectory(${CMAKE_SOURCE_DIR}/src)

add_executable(patchmatchstereo ${SRC})

IF(WIN32)
  set(CMAKE_EXE_LINK_DYNAMIC_CXX_FLAGS)
  set(CMAKE_SHARED_LIBRARY_CXX_FLAGS)
  set(CMAKE_SHARED_LIBRARY_LINK_CXX_FLAGS)
  set(CMAKE_EXE_LINK_DYNAMIC_C_FLAGS)
  set(CMAKE_SHARED_LIBRARY_C_FLAGS)
  set(CMAKE_SHARED_LIBRARY_LINK_C_FLAGS)
  set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -static")
ELSE()
  find_package(PCL REQUIRED COMPONENTS io visualization common)
  link_directories(${PCL_LIBRARY_DIRS})
  add_definitions(${PCL_DEFINITIONS})
  include_directories(${PCL_INCLUDE_DIRS})
  target_link_libraries(
    patchmatchstereo
    ${PCL_LIBRARIES}
  )
ENDIF()

include_directories(
  ${CMAKE_SOURCE_DIR}/include 
  ${OpenCL_INCLUDE_DIRS}
)

target_link_libraries(
  patchmatchstereo 
  ${OpenCV_LIBS} 
  ${OpenCL_LIBRARIES}
)

install(TARGETS patchmatchstereo RUNTIME DESTINATION bin)
