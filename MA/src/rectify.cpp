// Copyright (C) 2015 Dominik Wetzel <dominik.wetzel@fh-zwickau.de>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "rectify.h"
#include "clickimage.h"
#include "helper.h"
#include "qer/qer.h"
#include "pollefey/pollefey.h"
#include "statistic.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#define SIFT false
// #define ASIFTSIFT

const std::string CALIB = ""; // "../data/calibration/camera_Nikon.xml";
std::vector<cv::Point2f> oneMatch;
std::string package;

Rectification::Rectification(cv::InputOutputArray _img1, cv::InputOutputArray _img2, std::string output_dir,std::string asift_path, bool manual, Statistic* _stats, unsigned matches, bool useOuter) 
  : manual(manual), useOuter(useOuter), output_dir(output_dir), asift_path(asift_path), img1(_img1.getMat()), img2(_img2.getMat()), imgSize(_img1.size())
{
  this->stats = !_stats ? new Statistic() : _stats;
  package = std::string(manual ? "MAN" : "ASIFT") + std::string(useOuter ? "wOut" : "nOut");
  cimg = new ClickImage(_img1, _img2, output_dir); 
  std::string matchfile = computeMatches(manual, matches);
  removeOutliers();
  writePackage();
#ifdef ASIFTSIFT
  bool sift = SIFT;
  std::ifstream tmp(matchfile);
  std::string all = "",time;
  while(!tmp.eof())
  {
    if(all.empty())
    {
      std::getline(tmp,all);
      continue;
    }
    std::getline(tmp,time);
    if(time.find("s") < std::string::npos)
      break;
  }
  
  stats->writeLine(std::string(sift ? "" : "A") + "SIFT:\t" + std::to_string(points1.size().area()) + "\t" + all + "\t" + time);
#endif
}

void Rectification::writePackage()
{
  cv::Mat img,tmp;
  Helper::makeTwoImagesOne(img1,img2,img);
  Helper::writeMatches(points1,points2,img,tmp);
  std::string outFile = output_dir + "matches_inner.png";
  cv::imwrite(outFile,tmp);
  Helper::addTo7zFile(output_dir,outFile,package);
  if(useOuter)
  {
    Helper::writeMatches(out_points1,out_points2,img,tmp);
    outFile = output_dir + "matches_outer.png";
    cv::imwrite(outFile,tmp);
    Helper::addTo7zFile(output_dir,outFile,package);
  }
}


void Rectification::getOneMatch(std::vector< cv::Point2f >& match)
{
  match[0] = oneMatch[0];
  match[1] = oneMatch[1];
  match[2] = oneMatch[2];
  match[3] = oneMatch[3];
}

void Rectification::computeRectifiedPoints(std::vector<cv::Point2f> &points, cv::InputArray Hmapx, cv::InputArray mapy)
{
  if(Hmapx.size() == cv::Size(3,3) && mapy.size().width == 0) {
    cv::perspectiveTransform(points,points,Hmapx);
  } else {
    Helper::remapPoints(points,points,imgSize,Hmapx,mapy);
  }
}

void Rectification::drawLines(cv::InputArray src, cv::OutputArray dst, std::vector<cv::Point2f> &points)
{  
  cv::Mat image;
  float f = 2*M_PI / points.size();
  src.getMat().copyTo(image);
  for(std::size_t i = 0; i < points.size(); i += 2) {
    int r = sin(f*i + 0) * 127 + 128;
    int g = sin(f*i + 2*M_PI/3) * 127 + 128;
    int b = sin(f*i + 4*M_PI/3) * 127 + 128;
    cv::line(image,points[i],points[i+1],cv::Scalar(r,g,b));
  }
  image.copyTo(dst);
}

// For test purposes I use an uncalibrated intrinsic matrix (with Meta-Information from the image)
void Rectification::getIntrinsicAndDistortion(cv::OutputArray K_, cv::OutputArray dist_, std::string file)
{
  cv::Mat _K;
  if(file.empty()) 
  {
//     double fx = 35; double fy = 35;
    double fx = 55; double fy = 55;
//     double fx = 105; double fy = 105;
//     double fx = 136; double fy = 136;
//     double fx = 137; double fy = 137;
//     double fx = 150; double fy = 150;
//     double fx = 200; double fy = 200;
//     double fx = 500; double fy = 500;
//     double fx = 1000; double fy = 1000;
    double cx = (imgSize.width - 1) * 0.5; double cy = (imgSize.height - 1) * 0.5;
    double k1 = 0.0; double k2 = 0.0; double k3 = 0.0; double p1 = 0.0; double p2 = 0.0;
    
    double cam[9] = {fx,0,cx,0,fy,cy,0,0,1};
    K_.create(3,3,CV_64F);
    _K = K_.getMat();
    for(int i = 0; i < 9 ; ++i) {
      _K.at<double>(i / 3, i % 3) = cam[i];
    }
    double d[5] = {k1, k2, p1, p2, k3};
    dist_.create(5,1,CV_64F);  
    cv::Mat dist = dist_.getMat();
    for(int i = 0; i < 5 ; ++i) {
      dist.at<double>(i,0) = d[i];
    }
  }
  else
  {
    K_.create(3,3,CV_64F);
    dist_.create(5,1,CV_64F);
    _K = K_.getMat();
    cv::Mat dist = dist_.getMat();
    cv::FileStorage fs(file, cv::FileStorage::READ);
    fs["Camera_Matrix"] >> _K;
    fs["Distortion_Coefficients"] >> dist;
    fs.release();
  }
  K = _K.clone();
}

void Rectification::removeShearing(cv::InputOutputArray homography)
{
  cv::Mat H = homography.getMat();
  int w = imgSize.width;
  int h = imgSize.height;
  cv::Point2f p,q;
  double k1,k2;
  std::vector<cv::Point2f> pts;
  pts.push_back(cv::Point2f((w-1.f)/2.f, 0.f));
  pts.push_back(cv::Point2f(w-1.f, (h-1.f)/2.f));
  pts.push_back(cv::Point2f((w-1.f)/2.f, h-1.f));
  pts.push_back(cv::Point2f(0.f, (h-1.f)/2.f));
  cv::perspectiveTransform(pts,pts,H);
  p = pts[1] - pts[3];
  q = pts[2] - pts[0];
  k1 = (h*h*p.y*p.y + w*w*q.y*q.y)/(h*w*(p.y*q.x - p.x*q.y));
  k2 = (h*h*p.x*p.y + w*w*q.x*q.y)/(h*w*(p.x*q.y - p.y*q.x)); 
  cv::Mat S = cv::Mat::eye(3,3,CV_64FC1);
  S.at<double>(0,0) = -k1;
  S.at<double>(0,1) = -k2;
  H = S*H;
}

float Rectification::pointDistance(cv::Point2f p1, cv::Point2f p2, int method)
{
  float dx = fabs(p2.x - p1.x), dy = fabs(p2.y - p1.y);
  if(method == DistMethod::D1) return dx + dy;
  else if(method == DistMethod::ONLY_Y) return dy;
  else if(method == DistMethod::D_INF) return dx > dy ? dx : dy; // manhatten distance
  else return sqrt(dx * dx + dy * dy); // euclidean distance
}

float Rectification::avgEpipolarLineDistance(int img, int method)
{
  cv::Mat lines, p1, p2;
  p1 = img == 1 ? points1 : points2;
  p2 = img == 1 ? points2 : points1;
  cv::computeCorrespondEpilines(p1,img,F,lines);
  float sum = 0;
  float max = 0;
  float min = INFINITY;
  int size = lines.rows;
  for(int i = 0; i < size; ++i) 
  {
    cv::Vec3f l = lines.at<cv::Vec3f>(i);
    cv::Point2f p = p2.at<cv::Point2f>(i), np;
    float x = (-p.y*l[0]*l[1]+l[1]*l[1]*p.x - l[2]*l[0])/(l[0]*l[0] + l[1]*l[1]);
    float y = -(l[2] + l[0]*x)/l[1];
    np = cv::Point2f(x,y);
    float d = pointDistance(p,np);
    max = d > max ? d : max;
    min = d < min ? d : min;
    sum += d;
  }
  //std::cout << " | Interval: [" << min << ";" << max << "]" << std::endl;
  return sum / size;
}

std::tuple<float,int> Rectification::avgRectificationError(cv::InputArray points1, cv::InputArray points2, std::vector<cv::Mat>& maps) 
{
  if(points1.size().area() < 1) return std::tuple<float,int>(INFINITY,0);
  std::vector<cv::Point2f> p1, p2;
  p1 = points1.getMat().clone();
  p2 = points2.getMat().clone();
  if(maps[0].size() == cv::Size(3,3)) {
    computeRectifiedPoints(p1,maps[0]);
    computeRectifiedPoints(p2,maps[1]);
    Helper::checkBounds(p1,p2,imgSize);
  } else {
    computeRectifiedPoints(p1,maps[0],maps[1]);
    computeRectifiedPoints(p2,maps[2],maps[3]);
    Helper::checkBounds(p1,p2,maps[0].size());
  }
  float err = 0;
  int size = p1.size();
  bool first = true;
  for(int i = 0; i < size; ++i) {
    float t_err = pointDistance(p1[i],p2[i],DistMethod::ONLY_Y);
    if(first && t_err < 0.5)
    {
      oneMatch.push_back(p1[i]);
      oneMatch.push_back(p2[i]);
    }
    err += t_err;
  }
  return std::tuple<float,int>(size == 0 ? INFINITY : err / size, size);
}

clock_t Rectification::calculateF(std::vector<cv::Mat>& data, std::string algorithm)
{
  clock_t start = std::clock();
  if(algorithm == F_CALIB_5POINT) {
    static cv::Mat cam, dist_cam, R, T, E;
    static clock_t duration = 0;
    
    bool useData = data.size() == 4;
    if(cam.empty()) {
      getIntrinsicAndDistortion(cam, dist_cam, CALIB);
      find(points1, points2, R, T, E, cam,1);
    }
    F = cam.inv().t() * E * cam.inv(); 
    if(duration == 0) duration = std::clock() - start;
    if(useData) {
      cam.copyTo(data[0]);
      dist_cam.copyTo(data[1]);
      R.copyTo(data[2]);
      T.copyTo(data[3]);
    }
    return duration;
  } else if (algorithm == F_QER) {
    cv::Mat none;
    bool useData = data.size() == 3;
    disparity_range = qer::rectify(
	points1,points2,
	F,
	imgSize,
	useData ? data[0] : none, 
	useData ? data[1] : none,
	useData ? data[2] : none
    );
    F = F.t();
  } else {
    F = cv::findFundamentalMat(points1,points2,CV_FM_RANSAC,1);
  }
  return std::clock() - start;
}

clock_t Rectification::rectification(const std::string algorithm, const std::string f_algorithm, std::vector<cv::Mat>& H)
{
  clock_t ftime, start, duration;
  std::vector<cv::Mat> empty;
  std::string f_algo = f_algorithm == F_AUTO ? F_8POINT : f_algorithm;
  if(algorithm == HARTLEY)
  {
    ftime = calculateF(empty,f_algo);
    start = std::clock();
    cv::stereoRectifyUncalibrated(points1,points2,
				  F,
				  imgSize,
				  H[0],H[1]);
    removeShearing(H[0]);
    removeShearing(H[1]);
    duration = std::clock() - start + ftime;
  }
  
  else if (algorithm == QER)
  {
    std::vector<cv::Mat> vec;
    vec.push_back(cv::Mat());
    vec.push_back(cv::Mat());
    vec.push_back(cv::Mat());
    f_algo = F_QER;
    ftime = calculateF(vec,f_algo);
    duration = ftime;
    
    vec[0].copyTo(H[0]);
    vec[1].copyTo(H[1]);
    
    removeShearing(H[0]);
    removeShearing(H[1]);
    
    vec[2].copyTo(K);   
  } else if (algorithm == POLLEFEY) {
    ftime = calculateF(empty,f_algo);
    start = std::clock();
    pollefey::rectify(points1,points2,
		      F,
		      imgSize,
		      H[0],H[1],H[2],H[3]);
    duration = std::clock() - start + ftime;  
  } else if (algorithm == BOUGET) {
    f_algo = F_CALIB_5POINT;
    cv::Mat Rl,Rr,Pl,Pr,Q, cam, dist_cam;
    std::vector<cv::Mat> data;
    data.push_back(cv::Mat()); // camera matrix
    data.push_back(cv::Mat()); // distorion coefficents
    data.push_back(cv::Mat()); // rotation matrix
    data.push_back(cv::Mat()); // translation matrix
    
    ftime = calculateF(data,f_algo);
    start = std::clock();
    cam = data[0];
    dist_cam = data[1];
    cv::stereoRectify(cam,dist_cam,cam,dist_cam,imgSize, data[2],data[3], Rl, Rr, Pl, Pr, Q);
    cv::initUndistortRectifyMap(cam,dist_cam,Rl,Pl,imgSize,CV_32FC1,H[0],H[1]);
    cv::initUndistortRectifyMap(cam,dist_cam,Rr,Pr,imgSize,CV_32FC1,H[2],H[3]);
    duration = std::clock() - start + ftime;  
  } else {
    throw "Unknown Algorithm";
  }
  
  std::cout << algorithm + f_algo << ":" << std::endl;
  return duration;
}

void Rectification::rectifyImages(std::vector<cv::Mat> H, std::string prefix, bool show, bool override)
{
  std::vector<cv::Point2f> points_l, points_r;
  std::vector<cv::Vec3f> lines_l,lines_r;
  cv::Mat left,right, epi;
      
  Helper::computeEpilines(imgSize,F,lines_l, lines_r);
  Helper::computeLinePoints(imgSize,lines_l,points_l);
  Helper::computeLinePoints(imgSize,lines_r,points_r);
  drawLines(img1,left,points_l);
  drawLines(img2,right,points_r);
  
  Helper::makeTwoImagesOne(left,right,epi);
  if(! prefix.empty())
  {
    cv::imwrite(output_dir + prefix + "_unrect.png",epi);
    Helper::addTo7zFile(output_dir,output_dir + prefix + "_unrect.png", package);
  }
  if(show) Helper::showImage("Epilines",epi,true);
  if(H[0].size().area() > 9) {
    cv::remap(img1,left,H[0],H[1],CV_INTER_CUBIC);
    cv::remap(img2,right,H[2],H[3],CV_INTER_CUBIC);
  } else {
    cv::warpPerspective(img1,left,H[0],imgSize);
    cv::warpPerspective(img2,right,H[1],imgSize);
  }
  if(override) {left.copyTo(img1); right.copyTo(img2);}
  
  ClickImage cimg = ClickImage(left,right,"");
  cv::Mat f = cv::Mat::zeros(3,3,CV_64F);
  f.at<double>(2,1) = 1;
  f.at<double>(1,2) = -1;
  if(show) cimg.clickLines(f);
  
  cv::Size newImgSize = left.size();
  Helper::computeEpilines(newImgSize,f,lines_l, lines_r);
  Helper::computeLinePoints(imgSize,lines_l,points_l);
  Helper::computeLinePoints(imgSize,lines_r,points_r);
  drawLines(left,left,points_l);
  drawLines(right,right,points_r);
  Helper::makeTwoImagesOne(left,right,epi);
  if(! prefix.empty())
  { 
    cv::imwrite(output_dir + prefix + "_rect.png",epi);
    Helper::addTo7zFile(output_dir,output_dir + prefix + "_rect.png",package);
  }
  if(show) Helper::showImage("Epilines",epi,true);
}

cv::Mat Rectification::rectify(const std::string algorithm, std::string f_algorithm, bool override)
{
  if(firstRun){
    stats->writeRectifyHeader(points1,out_points1,rectangle,manual);
    firstRun = false;
  }
  std::string prefix = algorithm;
  if(algorithm == POLLEFEY || algorithm == HARTLEY) prefix.append(f_algorithm);
  
  std::vector<cv::Mat> H(4);
//   H.push_back(cv::Mat());
//   H.push_back(cv::Mat());
//   H.push_back(cv::Mat());
//   H.push_back(cv::Mat());
  double tTime = rectification(algorithm,f_algorithm,H) / (double) CLOCKS_PER_SEC;
  float epiError = (avgEpipolarLineDistance(1) + avgEpipolarLineDistance(2)) / 2;
  std::tuple<float,int> inlier = avgRectificationError(points1,points2,H);
  std::tuple<float,int> outlier = avgRectificationError(out_points1,out_points2,H);
  stats->writeRectStat(prefix,epiError,points1, out_points1, inlier, outlier,tTime);
  
//   std::cout << "Average reprojection error: " << epiError << std::endl;
//   std::cout << "Average inlier rectification error: " << std::get<0>(inlier) << " with " << std::get<1>(inlier) << "/" << points1.size().area() << " matches" <<   std::endl;
//   std::cout << "Average outlier rectification error: " << std::get<0>(outlier) << " with " << std::get<1>(outlier) << "/" << out_points1.size().area() << " matches" <<   std::endl;
//   cimg->clickLines(F);
  rectifyImages(H,prefix,false,override);
  return F;
}

std::string Rectification::computeMatches(bool manual, unsigned n_matches)
{  
  if(manual) {
    int matches = cimg->checkFile(false);
    if(matches <= 0 || (n_matches > 0 && !cimg->useOld())) {
      while(true){
	matches = cimg->clickpoints(true);
	std::cout << matches << std::endl;
	if(matches >= n_matches){ break;}
      }
      cimg->savePoints();
    }
    std::vector<cv::Point2f> clicked = std::vector<cv::Point2f>();
    cimg->getClicked(clicked);
    for(int i = 0; i < matches * 2; i += 2) {
      points1.push_back(clicked[i]);
      points2.push_back(clicked[i+1]);
    }
  
    int out_of_plane = cimg->checkFile(false,"outMatches");
    if(out_of_plane <= 0) {
      out_of_plane = cimg->clickpoints(false);
      cimg->savePoints("outMatches");
    }
    cimg->getClicked(clicked);
    for(int i = 0; i < out_of_plane * 2; i += 2) {
      if(useOuter)
      {
	out_points1.push_back(clicked[i]);
	out_points2.push_back(clicked[i+1]);
      }
      else
      {
	points1.push_back(clicked[i]);
	points2.push_back(clicked[i+1]);
      }
    }
    return "";
  }
  std::string asiftIm0 = output_dir + "im0.png";
  std::string asiftIm1 = output_dir + "im1.png";
  
  cv::imwrite(asiftIm0, img1);
  cv::imwrite(asiftIm1, img2);
  
  bool sift = SIFT;
  
  std::string outpathMatchings = output_dir + (sift ? "" : "a") + "siftMatchings.txt";
  std::ifstream smatchings(outpathMatchings.c_str());
  bool matches_exist = smatchings.good();
    
  //compute ASIFT matches
  if(!matches_exist) {
    smatchings.close();
    
    std::string asiftMask0 = output_dir + "mask0.png";
    std::string asiftMask1 = output_dir + "mask1.png";
    
    std::string asiftOutVert = output_dir + (sift ? "" : "a") + "siftOutVert.png";
    std::string asiftOutHori = output_dir + (sift ? "" : "a") + "siftOutHori.png";
    
    std::string outpathKeys0 = output_dir + (sift ? "" : "a") + "siftKeys1.txt";
    std::string outpathKeys1 = output_dir + (sift ? "" : "a") + "siftKeys2.txt";
    
    cv::Mat mask = cv::Mat::zeros(imgSize, CV_8UC3);
    // write out empty masks (=> we match across entire images)
    cv::imwrite(asiftMask0, mask);
    cv::imwrite(asiftMask1, mask);
        
    time_t start, end;
    std::time(&start);
    system((asift_path + " " +
    asiftIm0 + " " +
    asiftIm1 + " " +
    asiftMask0 + " " +
    asiftMask1 + " " +
    asiftOutVert + " " +
    asiftOutHori + " " +
    outpathMatchings + " " +
    outpathKeys0 + " " +
    outpathKeys1 + " " +
    (!sift ? "" : "a")).c_str());
    std::time(&end);
    
    std::ofstream app(outpathMatchings.c_str(),std::ios::app);
    app << std::to_string(std::difftime(end,start)) << "s" << std::endl;
    app.close();
    
    smatchings.open(outpathMatchings.c_str());
  }
  
  int numMatches;
  
  std::string line;
  std::vector<std::string> tokens;
  
  std::getline(smatchings, line);
  Helper::tokenize(line, tokens);
  
  // get number of matches (first line)
  std::stringstream(tokens.at(0)) >> numMatches;
  
  std::vector<cv::Point2f> pts0,pts1;
  
  float x0, y0, x1, y1;
  while (true) {
    std::getline(smatchings, line);
    if(smatchings.eof()) 
      break;
    Helper::tokenize(line, tokens);
    if(tokens.size() < 4)
      continue;
    
    std::stringstream(tokens.at(0)) >> x0;
    std::stringstream(tokens.at(1)) >> y0;
    std::stringstream(tokens.at(2)) >> x1;
    std::stringstream(tokens.at(3)) >> y1;
    
    pts0.push_back(cv::Point2f(x0,y0));
    pts1.push_back(cv::Point2f(x1,y1));
  }
  
  if(useOuter)
    cimg->defineRectangle(rectangle, pts0, package);

  if(rectangle.empty()) {
    rectangle.push_back(cv::Point2f(0,0));
    rectangle.push_back(cv::Point2f(0,0));
  }
  float xl = rectangle[0].x;
  float xr = rectangle[1].x;
  float yu = rectangle[0].y;
  float yd = rectangle[1].y;
  
  for(int i = 0; i < numMatches; ++i) {
    cv::Point2f pp0 = pts0[i];
    cv::Point2f pp1 = pts1[i];
    // Matches in Rectangle
    if(pp0.x > xl && pp0.x < xr && pp0.y > yu && pp0.y < yd)
    {
      out_points1.push_back(pp0);
      out_points2.push_back(pp1);
    }
    else
    {
      points1.push_back(pp0);
      points2.push_back(pp1);
    }
  }
  
  std::cout << "Matches in rectangle as outer: ["<< points1.size().area() << "/" << numMatches << "]" << std::endl;
  
  smatchings.close();
  return outpathMatchings;
}

void Rectification::removeOutliers(float threshold)
{
  int image_width = imgSize.width;
  // initialization
  cv::Mat pts0, pts1;
  int inSize = points1.rows > points1.cols ? points1.rows : points2.cols;
  points1.copyTo(pts0);
  points2.copyTo(pts1);
  pts0.push_back(out_points1);
  pts1.push_back(out_points2);  
  points1 = cv::Mat();
  points2 = cv::Mat();
  out_points1 = cv::Mat();
  out_points2 = cv::Mat();
  
  int n;
  if(pts0.rows > pts0.cols)
  {
    n = pts0.rows;
  }
  else 
  {
    cv::transpose(pts0,pts0); cv::transpose(pts1,pts1); n = pts1.rows;
  }
  
  //calculate the slope for every match
  std::vector< std::pair<float,int> > slopes;
  for(int i = 0; i < n; ++i)
  {
    cv::Point2f pointl = pts0.at<cv::Point2f>(i,0);
    cv::Point2f pointr = pts1.at<cv::Point2f>(i,0);
    float m = (pointr.y - pointl.y) / ((pointr.x + image_width) - pointl.x);
    float alpha = atan(m)/M_PI*180.0;
//     slopes.push_back(std::pair<float,int>(m,i));    
    slopes.push_back(std::pair<float,int>(alpha,i));
  }
  
  // Sort the slopes to get the median
  std::sort(slopes.begin(),slopes.end()); 
  float median = 0.0;
  if((n % 2) == 0) {
    median = (((std::pair<float,int>) slopes[n/2]).first + ((std::pair<float,int>) slopes[n/2 - 1]).first) / 2.0;
  } else {
    median = ((std::pair<float,int>) slopes[n/2]).first;
  }
  // sort to initial order
  auto cmpBack = [](std::pair<float,int> const & a, std::pair<float,int> const & b){
    return a.second < b.second;
  };
  std::sort(slopes.begin(),slopes.end(),cmpBack);
  
  std::vector<std::pair<float, int> > outliers;

  // remove outliers
  int out_count = 0;
  for(std::pair<float,int> slope : slopes) {
    bool in = slope.second < inSize;
    cv::Mat *p1 = in ? &points1 : &out_points1;
    cv::Mat *p2 = in ? &points2 : &out_points2;
    if(fabs(slope.first - median) > threshold) 
    {
      ++out_count;
      outliers.push_back(slope);
    } 
    else
    {
      p1->push_back(cv::Point2f(pts0.at<cv::Point2f>(slope.second).x,pts0.at<cv::Point2f>(slope.second).y));
      p2->push_back(cv::Point2f(pts1.at<cv::Point2f>(slope.second).x,pts1.at<cv::Point2f>(slope.second).y));
    }
  }
  // resize the matrix to appropriate size
  oneMatch.push_back(points1.at<cv::Point2f>(0));
  oneMatch.push_back(points2.at<cv::Point2f>(0));
  
  std::cout << "Removed " << out_count << " outliers in " << n << " matches." << std::endl;
// #define DEBUG
#ifdef DEBUG
//   float alpha = atan(median)/M_PI*180.0;
  float m = tan(median*M_PI/180.0);
  std::cout << "Median: " << m << " << Anstieg | Winkel >> " << median << std::endl;
  std::cout << "Outliers: " << std::endl;
  for(std::pair<float,int> slope : outliers) {
//     alpha = atan(slope.first)/M_PI*180.0;
    m = tan(slope.first*M_PI/180.0);
    std::cout << pts0.at<cv::Point2f>(slope.second) << ": " << m << " << Anstieg | Winkel >> " << slope.first << std::endl;
  }
  std::cout << "Initial size: " << pts0.size().area() << " Clean size: " << points1.size().area() << std::endl;
#endif
}


void Rectification::compareYStretch(const std::string algo1, const std::string algo2, const std::string f_algo)
{
  std::vector<cv::Mat> tmp(4), Hs(4);
  rectification(algo1,f_algo,tmp);
  Hs[0] = tmp[0].clone();
  Hs[1] = tmp[1].clone();
  rectification(algo2,f_algo,tmp);
  Hs[2] = tmp[0].clone();
  Hs[3] = tmp[1].clone();
  
  std::vector<cv::Point2f> pts(6), tmpPts;
  int h_height = imgSize.height / 2;
  int h_width = imgSize.width / 2;
  pts[0] = cv::Point2f(20,h_height - 10);
  pts[1] = cv::Point2f(20,h_height + 10);
  pts[2] = cv::Point2f(h_width,h_height - 10);
  pts[3] = cv::Point2f(h_width,h_height + 10);
  pts[4] = cv::Point2f(imgSize.width - 20,h_height - 10);
  pts[5] = cv::Point2f(imgSize.width - 20,h_height + 10);
  
  std::vector<float> sums(4,0.f),sumYs(4,0.f);
  for(int k = 0; k < Hs.size(); ++k)
  {
    cv::perspectiveTransform(pts,tmpPts, Hs[k]); 
    for(int i = 0; i < pts.size(); i += 2)
    {
//       std::cout << pts[i] << " " << pts[i+1] << " ";
//       std::cout << tmpPts[i] << " " << tmpPts[i+1];
      sums[k] += pointDistance(tmpPts[i],tmpPts[i+1]);
      sumYs[k] += pointDistance(tmpPts[i],tmpPts[i+1],DistMethod::ONLY_Y);
//       std::cout << " SumY" << sumYs[k] << std::endl;
    }
  }
  stats->writeLine("Point deviation (" + algo1 + "-" + algo2 + " with F-Mat=" + f_algo + "):" +
		   std::to_string((sums[0] - sums[2] + sums[1] - sums[3])/6) + 
		   " Y-Stretch: " + std::to_string((sumYs[0] - sumYs[2] + sumYs[1] - sumYs[3])/6));
}
