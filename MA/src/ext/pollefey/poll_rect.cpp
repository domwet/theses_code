// Copyright (C) 2015 Dominik Wetzel <dominik.wetzel@fh-zwickau.de>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include <iostream>
#include <stdio.h>
#include <fstream>

#include "pollefey/polarcalibration.h"
#include "pollefey/pollefey.h"
#include "helper.h"
#include "clickimage.h"

using namespace std;
namespace pollefey {
  
  // #define BASE_PATH "/local/imaged/stixels/bahnhof"
  // #define IMG1_PATH "seq03-img-left"
  // #define FILE_STRING1 "image_%08d_0.png"
  // #define IMG2_PATH "seq03-img-left"
  // #define FILE_STRING2 "image_%08d_0.png"
  // #define IMG2_PATH "seq03-img-right"
  // #define FILE_STRING2 "image_%08d_1.png"
  // #define CALIBRATION_STRING "cam%d.cal"
  // #define MIN_IDX 10//138 //120
  // #define MAX_IDX 999
  
  void rectify(cv::InputArray _points1, cv::InputArray _points2, cv::InputArray _F, cv::Size imgSize, cv::OutputArray _mapx1, cv::OutputArray _mapy1, cv::OutputArray _mapx2, cv::OutputArray _mapy2) {
    
    cv::Mat F, pts1, pts2, mapx1, mapy1, mapx2, mapy2;;
    pts1 = _points1.getMat();
    pts2 = _points2.getMat();
    F = _F.getMat().clone();
    
    vector<cv::Point2f> points1,points2;
    for(int i = 0; i < pts1.size().width; ++i) {
	points1.push_back(pts1.at<cv::Point2f>(i));
	points2.push_back(pts2.at<cv::Point2f>(i));
    }
    
    PolarCalibration calibrator;
    calibrator.toggleShowCommonRegion(false);
    calibrator.toggleShowIterations(false);
    
    calibrator.compute(imgSize,F,points1,points2);
    
    
    // Visualization
    calibrator.getMaps(mapx1,mapy1,1);
    calibrator.getMaps(mapx2,mapy2,2);
    
    _mapx1.create(mapx1.size(),mapx1.type());
    _mapy1.create(mapy1.size(),mapy1.type());
    _mapx2.create(mapx2.size(),mapx2.type());
    _mapy2.create(mapy2.size(),mapy2.type());
    
    mapx1.copyTo(_mapx1.getMat());
    mapy1.copyTo(_mapy1.getMat());
    mapx2.copyTo(_mapx2.getMat());
    mapy2.copyTo(_mapy2.getMat());
        
//     cv::Mat scaled1, scaled2;
//     int scale = 700;
//     cv::Size newSize;
//     if (rectified1.cols > rectified1.rows) {
//       newSize = cv::Size(scale, scale * rectified1.rows / rectified1.cols);
//     } else {
//       newSize = cv::Size(scale * rectified1.cols / rectified1.rows, scale);
//     }
//     
//     cout << "prevSize " << rectified1.size() << endl;
//     cout << "newSize " << newSize << endl;
//     
//     cv::resize(rectified1, scaled1, newSize);
//     cv::resize(rectified2, scaled2, newSize);
//     
//     Helper::makeTwoImagesOne(scaled1,scaled2, epi);
//     F = cv::Mat::zeros(3,3,F.type());
//     F.at<double>(1,2) = -1;
//     F.at<double>(2,1) = 1;
//     
//     ClickImage cimg = ClickImage(epi,"");
//     cimg.clickLines(F);
        
    //         cv::imshow("showImg2", scaled2);
    //         cv::moveWindow("showImg2", 700, 0);
    
//     cv::waitKey(0);
  }
}


