// Copyright (C) 2015 Dominik Wetzel <dominik.wetzel@fh-zwickau.de>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include <fstream>
#include <cmath>
#include "clickimage.h"
#include "helper.h"

static const std::string w_points = "Click Points, Press Enter";
static const std::string w_lines = "Click Points to show Epilines, Press Enter";
static const std::string w_rectangle = "Click 2 Points to create a rectangle";
static const std::string IMAGE_NAME = "manualMatches.png";
static const std::string TXT_NAME = "manualMatchings.txt";

void ClickImage::mouseHandler(int event, int x, int y, int flags, void* param)
{
  std::tuple<cv::Mat,std::vector<cv::Point2f>*,float > tuple = *((std::tuple<cv::Mat,std::vector<cv::Point2f>*,float >*) param); 
  cv::Mat image = std::get<0>(tuple);
  std::ostringstream text;
  text << cv::Point(x,y);
  cv::Point left = cv::Point(0,image.rows);
  cv::Point right = cv::Point(100,image.rows - 18);
  cv::Point txtPt = cv::Point(0,image.rows - 5);
  cv::rectangle(image,left,right,cv::Scalar(0),-1);
  cv::putText(image,text.str(),txtPt,CV_FONT_NORMAL,0.4,cv::Scalar(255,255,255));
  left.x = txtPt.x = image.cols -100;
  right.x = image.cols;
  cv::rectangle(image,left,right,cv::Scalar(0),-1);
  cv::putText(image,text.str(),txtPt,CV_FONT_NORMAL,0.4,cv::Scalar(255,255,255));
  
  cv::imshow(w_points,image);
  
  switch(event){
    case CV_EVENT_LBUTTONDOWN:
      // load data
      std::vector<cv::Point2f>* clicked = std::get<1>(tuple);
      float zoom = std::get<2>(tuple);
      
      // save clicked point
      clicked->push_back(cv::Point2f(x / zoom,y / zoom));
      int pos = clicked->size();
      
      // show clicked point and draw line for 2 points
      image.at<cv::Vec3b>(y,x) = cv::Vec3b(255,255,255);
      if(pos % 2 == 0) {
	cv::Point2f p1, p2;
	p1 = clicked->at(pos-2);
	p2 = clicked->at(pos-1);
	cv::line(image,cv::Point2f(p1.x * zoom, p1.y * zoom), cv::Point2f(p2.x * zoom, p2.y * zoom),cv::Scalar(255,255,255));
	// bring matches in the right order and normalize them
	int width = image.cols / (2 * zoom);
	if(p1.x >= width) {
	  clicked->at(pos-1) = cv::Point2f(p1.x - width, p1.y);
	  clicked->at(pos-2) = p2;
	} else {
	  clicked->at(pos-1) = cv::Point2f(p2.x - width, p2.y);
	}
	std::cout << "Match " << clicked->size() / 2 << ":" << clicked->at(pos-2) << clicked->back() <<  std::endl;

      }
      
      left.x = txtPt.x = 100;
      right.x = 200;
      cv::rectangle(image,left,right,cv::Scalar(0),-1);
      cv::putText(image,text.str(),txtPt,CV_FONT_NORMAL,0.4,cv::Scalar(255,0,0));
      left.x = txtPt.x = image.cols - 200;
      right.x = image.cols - 100;
      cv::rectangle(image,left,right,cv::Scalar(0),-1);
      cv::putText(image,text.str(),txtPt,CV_FONT_NORMAL,0.4,cv::Scalar(255,0,0));
      
      cv::imshow(w_points,image);
      break;
  }
}

void ClickImage::mouseHandlerLines(int event, int x, int y, int flags, void* param)
{
  std::vector<cv::Mat> vec = *((std::vector<cv::Mat> *) param); 
  cv::Mat image = vec[0];
  std::ostringstream text;
  text << cv::Point(x,y);
  cv::Point left = cv::Point(0,image.rows);
  cv::Point right = cv::Point(100,image.rows - 18);
  cv::rectangle(image,left,right,cv::Scalar(0),-1);
  cv::putText(image,text.str(),cv::Point(0,image.rows -5),CV_FONT_NORMAL,0.4,cv::Scalar(255,255,255));
  cv::imshow(w_lines,image);
  
  switch(event){
    case CV_EVENT_LBUTTONDOWN:
      // load data
      cv::Mat F = vec[1];
      cv::Size half = cv::Size(image.cols / 2, image.rows);
      int img, x1;
      
      // get the clicked image
      if(x >= half.width){
	img = 2;
	x1 = x - half.width;
      } else {
	img = 1;
	x1 = x;
      }
      
      std::vector<cv::Point2f> pts1,pts2,pts;
      std::vector<cv::Vec3f> line1, line2;
      pts.push_back(cv::Point2f(x1,y));
      Helper::computeEpilines(half,F,line1,line2,pts,img);
      Helper::computeLinePoints(half,line1,pts1);
      Helper::computeLinePoints(half,line2,pts2);
      
      if(img == 1) {
	cv::line(image, pts1[0],pts1[1],cv::Scalar(255,255,255));
	cv::line(image, cv::Point(pts2[0].x + half.width,pts2[0].y),cv::Point(pts2[1].x + half.width,pts2[1].y),cv::Scalar(255,255,255));
      } else {
	cv::line(image, pts2[0],pts2[1],cv::Scalar(255,255,255));
	cv::line(image, cv::Point(pts1[0].x + half.width,pts1[0].y),cv::Point(pts1[1].x + half.width,pts1[1].y),cv::Scalar(255,255,255));
      }
      cv::imshow(w_lines, image);
      break;
  }
}

void ClickImage::mouseHandlerRect(int event, int x, int y, int flags, void* param)
{
  std::tuple<cv::Mat, std::vector<cv::Point2f>*, cv::Mat> tuple = *((std::tuple<cv::Mat, std::vector<cv::Point2f>*, cv::Mat > *) param);
  cv::Mat image = std::get<0>(tuple).clone();
  std::vector<cv::Point2f>* rectangle = std::get<1>(tuple);
  cv::Point2f p2 = cv::Point2f(x,y);
  
  if(rectangle->size() == 1) {
    cv::Point2f p1 = rectangle->at(0);
    cv::rectangle(image,p1,p2,cv::Scalar(0,0,0));
    cv::imshow(w_rectangle,image);
  }
    switch(event){
    case CV_EVENT_LBUTTONDOWN:
      rectangle->push_back(p2);
      if(rectangle->size() > 2) {
	rectangle->clear();
	rectangle->push_back(p2);
	cv::imshow(w_rectangle,image);
      }
      if(rectangle->size() == 2) image.copyTo(std::get<2>(tuple));
      break;
    }
}

ClickImage::ClickImage(cv::InputArray _img1, cv::InputArray _img2, std::string outputDir):
  outputDir(outputDir)
{
  zoom = 1.5;
  Helper::makeTwoImagesOne(_img1,_img2,image);
  clicked = new std::vector<cv::Point2f>();
}

ClickImage::~ClickImage(void) 
{
  delete clicked;
}

void ClickImage::fillImage(cv::InputOutputArray img)
{
  std::vector<cv::Point2f> points = *clicked,left,right;
  for(std::size_t i = 0; i < points.size(); i += 2){
    left.push_back(points[i]);
    right.push_back(points[i+1]);
  }
  Helper::writeMatches(left,right,img,img);
}

bool ClickImage::useOld()
{
  cv::Mat img = image.clone();
  fillImage(img);
  std::string option = "Use these points? [Y/n]:";
  cv::namedWindow(option);
  cv::imshow(option, img);
  char key = cv::waitKey(0);
  cv::destroyWindow(option);
  return !(key == 'n' || key == 'N');
}

void ClickImage::writeImage(std::string file)
{
  cv::Mat img = image.clone();
  fillImage(img);
  cv::imwrite(outputDir + (file.empty() ? IMAGE_NAME : (file + ".png")),img);
}

int ClickImage::clickpoints(bool write, std::string file)
{
  clicked->clear();
  cv::Mat img;
  cv::resize(image,img,cv::Size(image.size().width * zoom, image.size().height * zoom));
  cv::namedWindow(w_points);
  std::tuple<cv::Mat,std::vector<cv::Point2f>*,float > tuple = std::tuple<cv::Mat,std::vector<cv::Point2f>*,float >(img,clicked,zoom);
  cv::setMouseCallback(w_points,ClickImage::mouseHandler,&tuple);
  cv::imshow(w_points,img);
  cv::waitKey(0);
  if(write)  writeImage(file);
  cv::destroyWindow(w_points);
  return clicked->size() / 2;
}

void ClickImage::defineRectangle(std::vector<cv::Point2f>& rectangle, std::vector<cv::Point2f>& pts0,const std::string& package)
{
  cv::Mat tmp,img = cv::Mat(image.rows, image.cols / 2, image.type());
  for(int i = 0; i < image.rows; ++i) {
    for(int j = 0; j < image.cols / 2; ++j) {
      img.at<cv::Vec3b>(i,j) = image.at<cv::Vec3b>(i,j);
    }
  }
  for(cv::Point2f point : pts0){
    img.at<cv::Vec3b>((int) round(point.y),(int) round(point.x)) = cv::Vec3b(255,255,255);
  }
  img.copyTo(tmp);
  
  std::tuple<cv::Mat, std::vector<cv::Point2f>*,cv::Mat > tuple = std::tuple<cv::Mat, std::vector<cv::Point2f>*,cv::Mat > (img,&rectangle,tmp);
  cv::namedWindow(w_rectangle);
  cv::setMouseCallback(w_rectangle,ClickImage::mouseHandlerRect,&tuple);
  cv::imshow(w_rectangle,img);
  cv::waitKey(0);
  cv::destroyWindow(w_rectangle);
  
  if(rectangle.size() != 2) {
    rectangle.clear();
  } else {
    cv::imwrite(outputDir + "choosenRect.png",tmp);
    if(!package.empty())
      Helper::addTo7zFile(outputDir, outputDir + "choosenRect.png",package);
    
    float x0 = rectangle[0].x, y0 = rectangle[0].y;
    float x1 = rectangle[1].x, y1 = rectangle[1].y;
    float temp;
    
    if( x0 > x1 ) {
      temp = x0; x0 = x1; x1 = temp;
    }
    if( y0 > y1 ) {
      temp = y0; y0 = y1; y1 = temp;
    }
    rectangle.clear();
    rectangle.push_back(cv::Point2f(x0,y0));
    rectangle.push_back(cv::Point2f(x1,y1));
  }
}

void ClickImage::getClicked(std::vector<cv::Point2f> &pts)
{
  pts.clear();
  for(cv::Point2f point : *clicked) {
    pts.push_back(cv::Point2f(point.x, point.y));
  }
}

void ClickImage::clickLines(cv::InputArray F_)
{
  cv::Mat img = image.clone();
  cv::Mat F = F_.getMat();
  std::vector<cv::Mat> vec;
  vec.push_back(img);
  vec.push_back(F);
  cv::namedWindow(w_lines);
  cv::setMouseCallback(w_lines,ClickImage::mouseHandlerLines,&vec);
  cv::imshow(w_lines,img);
  cv::waitKey(0);
  cv::destroyWindow(w_lines);
}

void ClickImage::savePoints(std::string file)
{
  std::string outFile = outputDir + (file.empty() ? TXT_NAME : (file + ".txt"));
  std::ofstream toSave(outFile.c_str());
  toSave << clicked->size()/2 << std::endl;
  for (std::size_t i = 0; i < clicked->size(); i+=2) {
    cv::Point2f p1 = clicked->at(i), p2 = clicked->at(i+1);
    toSave << p1.x << "  " << p1.y << "  " << p2.x << "  " << p2.y << std::endl;
  }
  toSave.close();
}

int ClickImage::checkFile(bool write, std::string file) 
{
  std::string inFile = outputDir + (file.empty() ? TXT_NAME : (file + ".txt"));
  std::ifstream toRead(inFile.c_str());
  int matches = 0;
  if(toRead.good()){
    clicked->clear();
    std::string line;
    std::vector<std::string> tokens;
    std::getline(toRead, line);
    float x0,y0,x1,y1;
    std::stringstream(line) >> matches;
    while(true){
      std::getline(toRead, line);
      if(toRead.eof()) break;
      Helper::tokenize(line, tokens);
    
      std::stringstream(tokens.at(0)) >> x0;
      std::stringstream(tokens.at(1)) >> y0;
      std::stringstream(tokens.at(2)) >> x1;
      std::stringstream(tokens.at(3)) >> y1;
      
      clicked->push_back(cv::Point2f(x0,y0));
      clicked->push_back(cv::Point2f(x1,y1));
    }
    if(write) writeImage(file);
  }
  toRead.close();
  return matches;
}

