// Copyright (C) 2015 Dominik Wetzel <dominik.wetzel@fh-zwickau.de>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "patchmatch.h"
#include "open_cl_stuff.h"

PatchMatch::PatchMatch(cv::InputArray _left, cv::InputArray _right, std::map<std::string,float>& options, Statistic* stats) :
  imgW(_left.size().width), imgH(_left.size().height)
{
  readOptions(options);
  stat = stats ? stats : new Statistic();
  vnidx3 = MAXNEIGHBOR * full_around;
  vnidx4 = vnidx3*imgW;
  
  int area = imgH*imgW;
  
  viewneighbors = PlaneVector(imgH*vnidx4);
  neighborCount = std::vector<int>(area);
  
  cv::medianBlur(_left,smooth_left,3);
  cv::medianBlur(_right,smooth_right,3);
  
  left_planes = PlaneVector(area);
  right_planes = PlaneVector(area);
  left_costs = FloatVector(area);
  right_costs = FloatVector(area);
  
  exps_color = FloatVector(3*255);
  precomputeexps();
  
  computegradient(_left,left);
  computegradient(_right,right);
  grayscale = 255 / (max_disp - min_disp);
  bordercosts = ((1.f - alpha) * tau1 + alpha * tau2) * bordercostspercentage;
  // Initialize Pointers on Left side
  changePointers(true);
  initAdditionalData();
}

PatchMatch::~PatchMatch()
{
  if(stat->isDummy()) 
    delete stat;
}

bool PatchMatch::solve(bool ocl, bool readOnly)
{     
  std::cout << "Starting PatchMatch" << std::endl;
  omp_set_num_threads(cpu_count);
  if(!readOnly)
  {
    if(ocl)
    {
      OCL _ocl = OCL();
      solvePatchMatch(&_ocl);
    }
    else
      solvePatchMatch(NULL);    
    std::cout << "Write Planes..." << std::endl;
    writePlanes();
  }
  else
  {
    readPlanes();
    // do not use psi for this
    around_cost = 0.f;
    changePointers(false);
    calculateCosts();
    changePointers(true);
    calculateCosts();
    stat->writeEnergy(iterations,left_costs,right_costs);
    stat->writeHistogram(left_costs,right_costs);
  }
  std::cout << "Postprocessing..." << std::endl;
  postprocess();
  solved = true;
  std::cout << "PatchMatch finished!" << std::endl;
  return solved;
}

void PatchMatch::solvePatchMatch(OCL* ocl)
{
  // Initialization    
  randomInit(ocl);
  changePointers(false); // Right
  randomInit(ocl);
  std::cout << "initialization done" << std::endl;
  
  stat->writeEnergy(0,left_costs,right_costs);
  std::string neighbor_out = "neighbors" + output_ext + ".png";
  std::string viewNeighbors_out = "neighbors_view" + output_ext + ".png";
  stat->createIterImages(neighbor_out,iterations,cv::Size(imgW*2,imgH),CV_8UC3);  
  stat->createIterImages(viewNeighbors_out, iterations, cv::Size(imgW*2,imgH), CV_8UC1);
  
  for(int it = 0; it < iterations; ++it)
  {
    std::cout << "Iteration: " << it << std::endl;
    int even = it & 1;
    int start_x = (imgW - 1) * even;
    int end_x = (imgW) * (1 - even) - 1*even;
    int change = 1 - 2*even;
    int lines = around;
    for(int curview = 1; curview >= 0; --curview)
    {
      std::cout << "View: " << curview << std::endl;
      changePointers(curview == 0); // Right, Left, Right ...
      prepareViewPropagation(lines);
      stat->saveNeighborCount(viewNeighbors_out, it,curview,neighborCount);
      if(ocl)
      {
	int type = 2;
	solveOneImage(it,even,change,start_x,end_x,lines,type,ocl);
      }
      else
	solveOneImage(it,even,change,start_x,end_x);
    }
    std::cout << "Save NeighborImage" << std::endl;
    stat->saveNeighbor(neighbor_out,it,cv::Size(imgW,imgH),left_planes,right_planes);
    std::cout << "Write Energies" << std::endl;
    stat->writeEnergy(it + 1,left_costs,right_costs);
    std::cout << "Write disp image" << std::endl;
    writeDispMap();
  }
  std::cout << "Write NeighborImage" << std::endl;
  stat->writeIterImage(neighbor_out);
  std::cout << "Write ViewNeighborImage" << std::endl;
  stat->writeIterImage(viewNeighbors_out);
  std::cout << "Write Histogram" << std::endl;
  stat->writeHistogram(left_costs,right_costs); 
}

void PatchMatch::solveOneImage(int it, int even, int change, int start_x, int end_x)
{
  #pragma omp parallel for
  for(int iter_y = 0; iter_y < imgH; ++iter_y)
  {
    int y = even*(imgH - 1) + change * iter_y;
    
    for(int x = start_x; x != end_x; x += change)
    {
      Plane new_plane, cur_plane = getPlane(x,y);
      float bestcosts = getCost(x,y);
      // spatial lower/upper
      int ny = y - change;
      bool err = false;
      if(ny >= 0 && ny < imgH)
      {
	new_plane = getPlane(x,ny);
	checkPlane(x,y,cur_plane,new_plane,bestcosts);
      }
      
      // spatial right/left
      int nx = x - change;
      if(nx >= 0 && nx < imgW)
      {
	new_plane = getPlane(nx,y);
	checkPlane(x,y,cur_plane,new_plane,bestcosts);
      }
      
      // ViewPropagation
      int count = neighborCount[y * imgW + x];
      int i_xy = y*vnidx4 + x*vnidx3;
      bool propagated = false;
      for(int i = 0; i < count; ++i)
      {
	int index = i_xy + i*full_around;
	for(int j = 0; j < full_around; ++j)
	{
	  new_plane = viewneighbors[index + j];
	  float y_disp = new_plane.y_disp;
	  if(y_disp - (int) y_disp > 0.1) 
	    continue;
	  if(checkPlane(x,y,cur_plane,new_plane,bestcosts,j-around))
	    propagated = true;
	}
      }
      
      if((refinement == Refinement::FULL) ||
	(refinement == Refinement::ROF && it == 0) ||
	(refinement == Refinement::ROINP && !propagated))
      {
	//planeRefinement
	float max_delta_z = max_disp + 1.f;//2.f;
	float max_delta_angle = 1.f;
	while(max_delta_z > 0.1f)
	{
	  new_plane = getrandomplane(x,y,&cur_plane,max_delta_z,max_delta_angle);
	  checkPlane(x,y,cur_plane,new_plane,bestcosts);
	  
	  max_delta_z /= 2.f;
	  max_delta_angle /= 2.f;
	}
      }
      
      getPlane(x,y) = cur_plane;
      getCost(x,y) = bestcosts;
    }
  }
}

void PatchMatch::solveOneImage(int it, int even, int change, int start_x, int end_x, int lines, int type, OCL* ocl)
{
  setAdditionalData(randNorm,change,lines);
  ocl->initBuffers(cur_img,other_img,cur_smooth,cur_planes,cur_costs,exps_color,additionalData,&viewneighbors,&neighborCount);
  if(!type)
  {
    std::cout << "whole image" << std::endl;
    ocl->runKernel(false,refinement,it);
  }
  else if(type == 1)
  {
    std::cout << "column-by-column" << std::endl;
    for(int x = start_x; x != end_x; x += change)
    {
      ocl->runKernel(false,refinement,it,x,-1);
    }
  }
  else if(type == 2)
  {
    std::cout << "row-by-row" << std::endl;
    for(int iter_y = 0; iter_y < imgH; ++iter_y)
    {
      int y = even*(imgH - 1) + change * iter_y;  
      ocl->runKernel(false,refinement,it,-1,y);
    }
  }
  else
  {
    std::cout << "point-by-point" << std::endl;
    for(int iter_y = 0; iter_y < imgH; ++iter_y)
    {
      int y = even*(imgH - 1) + change * iter_y;  
      for(int x = start_x; x != end_x; x += change)
      {
	ocl->runKernel(false,refinement,it,x,y);
      }
    }
  }
  ocl->readBuffers(cur_planes,cur_costs);
}

void PatchMatch::precomputeexps()
// computes a lookup table for exp for computational speed
{  
  for (int i = 0; i < exps_color.size(); i++)
  {
    exps_color[i] = exp ((float) i * -1.f / gamma_color);
  }
}

float PatchMatch::matchcosts(int px, int py, Plane& plane, float bestcosts, int around)
{
  float sum = 0.f;
  
  int start_x = px - windowsize / 2;
  int end_x   = px + windowsize / 2;
  int start_y = py - windowsize / 2;
  int end_y   = py + windowsize / 2;
  
  start_x = std::max(start_x, 0);
  end_x   = std::min(end_x, imgW);
  start_y = std::max(start_y, 0);
  end_y   = std::min(end_y, imgH);
  
  cv::Vec3b colorp = cur_smooth->at<cv::Vec3b>(py,px);
  
  for(int qy = start_y; qy < end_y; ++qy)
  {
    for(int qx = start_x; qx < end_x; ++qx)
    {
      float d = getD(qx,qy,plane);
      if(d < min_disp - 0.5f || d > max_disp + 0.5f) 
	return INFINITY;
      float qx1 = (float) qx + prefix * d;
      
      sum += colorweight(colorp,qx,qy) * pixelcosts(qx,qy,qx1,around);
      
      if(sum > bestcosts)
	return INFINITY;
    }
  }
  return sum;
}

std::pair<float,float> PatchMatch::getL1Dist(int x0, int y0, float x1f, int y1, cv::Mat* curImg, cv::Mat* otherImg)
{
  int x1 = (int) x1f;
  if(x1 < 0 || x1 > imgW)
    return std::pair<float,float>(bordercosts,bordercosts);
  
  int channels = curImg->channels();
  uchar *data = curImg->data + y0 * curImg->cols * curImg->elemSize() + x0 * curImg->elemSize();
  uchar *match = otherImg->data + y1 * otherImg->cols * otherImg->elemSize() + x1 * otherImg->elemSize();
  float difCol = 0.f, difGrad = 0.f;
  float fraction = x1f - x1;
  
  for (int c = 0; c < channels; ++c)
  {
    float c_ref = (float) *data++;
    float c_match = (float) *match;
    float c_match_plus = (float) *(match + channels);
    ++match;
    
    float interpol = c_match * (1.f - fraction) + c_match_plus * fraction;
    
    if(c > 2)
      difGrad += abs (c_ref - interpol);
    else
      difCol += abs (c_ref - interpol);
  }
  return std::pair<float,float>(difCol / (float) (channels - 1), difGrad);
}

float PatchMatch::colorweight(cv::Vec3b& cp, int qx, int qy)
{
  cv::Vec3b cq = cur_smooth->at<cv::Vec3b>(qy,qx);
  int cpq = abs(cp[0] - cq[0]) + abs(cp[1] - cq[1]) + abs(cp[2] - cq[2]);
  return exps_color[cpq];
}

float PatchMatch::pixelcosts(int x0, int y0, float x1, int around)
{
  int y1 = y0 + around;
  if(y1 < 0 || y1 >= imgH)
  {
    y1 = y0;
    around = 0;
  }
  std::pair<float,float> costs = getL1Dist(x0,y0,x1,y1,cur_img,other_img);
  float pixelscosts = (1.f - alpha) * std::min(costs.first,tau1) + alpha * std::min(costs.second,tau2);
  return pixelscosts + around_cost * pixelscosts * abs(around);
}

Plane PatchMatch::getrandomplane(int x, int y, Plane* c_plane, float max_delta_z, float max_delta_angle)
{
  float rand_max_half = RAND_MAX / 2.f;
  float z0,nx,ny,nz;
  float y_disp = 0;
  
  // random normal vector
  if(c_plane)
  {
    z0 = c_plane->z0 + ((float) rand() - rand_max_half) / rand_max_half * max_delta_z;
    nx = c_plane->nx + ((float) rand() - rand_max_half) / rand_max_half * max_delta_angle;
    ny = c_plane->ny + ((float) rand() - rand_max_half) / rand_max_half * max_delta_angle;
    nz = c_plane->nz + ((float) rand() - rand_max_half) / rand_max_half * max_delta_angle;
    y_disp = c_plane->y_disp;
  }
  else
  {
    float disp_range = max_disp - min_disp;
    z0 = min_disp + (float) rand() / RAND_MAX * disp_range;
    nx = ((float) rand() - rand_max_half) / imgW;
    ny = ((float) rand() - rand_max_half) / imgH;
    nz = ((float) rand() - rand_max_half) / disp_range;
  }
  
  // buid unit vector
  float length = sqrt (nx * nx + ny * ny + nz * nz);
  nx /= length;
  ny /= length;
  nz /= length;
  
  Plane plane;
  plane.a = -1.f * nx / nz;
  plane.b = -1.f * ny / nz;
  plane.c = (nx * x + ny * y + nz * z0) / nz;
  
  plane.z0 = z0;
  plane.nx = nx;
  plane.ny = ny;
  plane.nz = nz;
  
  plane.y_disp = y_disp;
  
  return plane;
}

void PatchMatch::calculateCosts()
{
  #pragma omp parallel for
  for(int y = 0; y < imgH; ++y)
    for(int x = 0;x < imgW; ++x)
    {
      Plane plane = getPlane(x,y);
      getCost(x,y) = matchcosts(x,y,plane,INFINITY,plane.y_disp);
    }
}


void PatchMatch::randomInit(OCL* ocl)
{
  if(ocl)
  {
    setAdditionalData(1);
    ocl->initBuffers(cur_img,other_img,cur_smooth,cur_planes,cur_costs,exps_color,additionalData);
    ocl->runKernel(true);
    ocl->readBuffers(cur_planes,cur_costs);
  }
  else
  {
    #pragma omp parallel for
    for(int y = 0; y < imgH; ++y)
      for(int x = 0;x < imgW; ++x)
	getPlane(x,y) = getrandomplane(x,y);
    calculateCosts();
  }
}

void PatchMatch::changePointers(bool toLeft)
{
  if(!toLeft) 
  {
    prefix = 1;
    cur_img = &right;
    other_img = &left;
    
    cur_planes = &right_planes;
    other_planes = &left_planes;
    cur_costs = &right_costs;
    other_costs = &left_costs;
    
    cur_smooth = &smooth_right;
  }
  else
  {
    prefix = -1;
    cur_img = &left;
    other_img = &right;
    
    cur_planes = &left_planes;
    other_planes = &right_planes;
    cur_costs = &left_costs;
    other_costs = &right_costs;
    
    cur_smooth = &smooth_left;
  }
}

void PatchMatch::prepareViewPropagation(int around)
{
  int full_around = 2*around+1;
  std::fill(neighborCount.begin(), neighborCount.end(),0);
  
  for(int y = 0; y < imgH; ++y)
  {
    for(int x = 0; x < imgW; ++x)
    {
      Plane other = getPlane(x,y,false);
      int match = x - prefix * (int) (getD(x,y,other) + 0.5f);
      if(match < 0 || match >= imgW) 
	continue;
      
      int index = y * imgW + match;
      
      int count = neighborCount[index];
      if(count >= MAXNEIGHBOR)
	continue;
      neighborCount[index]++;
      index = y*vnidx4 + match*vnidx3 + count*full_around;
      
      for(int j = 0; j < full_around; ++j)
      {
	Plane new_plane;
	int y_disp = j - around;
	int ny = y + y_disp;
	if(ny < 0 || ny >= imgH)
	  new_plane.y_disp = y_disp + 0.125;
	else
	{
	  other = getPlane(x,ny,false);
	  
	  // invert plane
	  // d_r = ((- a * x_r - b * y - c) / (1 - a)) for positive values * -1
	  float back = 1.f - prefix * other.a;
	  float nx = new_plane.a = other.a / back;
	  float ny = new_plane.b = other.b / back;
	  new_plane.c = other.c / back;
	  
	  // ax + by + c = z
	  // ax + by - 1z + c  = 0
	  new_plane.z0 = getD(match, y, new_plane);
	  
	  // unit vector
	  float length = sqrt (nx * nx + ny * ny + 1.f);
	  new_plane.nx = nx / length;
	  new_plane.ny = ny / length;
	  new_plane.nz = -1.f / length;
	  
	  new_plane.y_disp = y_disp;
	}
	// to store whether used above or below	  
	viewneighbors[index + j] = new_plane;
      }
    }
  }
}

void PatchMatch::computegradient (cv::InputArray _img, cv::OutputArray _grad)
// computes the image gradient in x-direction
{
  _grad.create(imgH,imgW,CV_8UC4);
  cv::Mat img = _img.getMat(), grad = _grad.getMat(), grey;
  // convert image to greyscale
  cv::cvtColor(img,grey,CV_BGR2GRAY);
  grad = cv::Mat::zeros(grad.size(),grad.type());
  
  for (int y = 0; y < imgH; y++)
    for (int x = 0; x < imgW; x++)
    {
      int I_center = (int) grey.at<uchar>(y,x);
      
      int I_left;
      if (x != 0)
	I_left = (int) grey.at<uchar>(y,x - 1);
      else
	I_left = I_center;
      
      int I_right;
      if (x != imgW - 1)
	I_right = (int) grey.at<uchar>(y,x + 1);
      else
	I_right = I_center;
      
      int delta_x = I_right - I_left + 122;
      
      delta_x = MIN (delta_x, 255);
      delta_x = MAX (delta_x, 0);
      
      cv::Vec3b imgData = img.at<cv::Vec3b>(y,x);	
      grad.at<cv::Vec4b>(y,x) = cv::Vec4b(imgData[0],imgData[1],imgData[2],(uchar) delta_x);
    }   
}

void PatchMatch::readOptions(std::map<std::string,float>& options)
{
  if(options.count("refinment") > 0)
    refinement = (Refinement) options["refinment"];
  if(options.count("max_disp") > 0)
    max_disp = options["max_disp"];
  if(options.count("min_disp") > 0)
    min_disp = options["min_disp"];
  if(options.count("around") > 0)
  {
    around = options["around"];
    full_around = 2*around +1;
  }
  if(options.count("cpus") > 0)
    cpu_count = options["cpus"];
  if(options.count("iters") > 0)
    iterations = options["iters"];
  if(options.count("around_cost") > 0)
    around_cost = options["around_cost"];
}

void PatchMatch::postprocess()
{
  std::vector<int> invalids = std::vector<int>(2*imgW*imgH,0);
  pp_invalidatePixels(invalids);
  pp_fill(invalids);
  std::cout << "WHOOT" << std::endl;
  writeDispMap();
}

void PatchMatch::pp_fill(std::vector<int>& invalids)
{
  Plane dummy;
  dummy.a = dummy.b = 0.f; 
  dummy.c = 99999.f;
  dummy.y_disp = 0.125;
  
  //     cv::Mat img = cv::Mat::zeros(imgH,2*imgW,CV_8UC1);
  //     for(int y = 0; y < imgH; ++y)
  //       for(int x = 0; x < 2*imgW; ++x)
  // 	img.at<uchar>(y,x) = (uchar) (invalids[y * imgW + x]*255);
  //       
  //     Helper::showImage("Invalids",img,true);
  
  for(int curview = 0; curview <= 1; ++curview)
  {
    changePointers(curview == 0);
    // left right fill planes
    for(int y = 0; y < imgH; ++y)
    {
      PlaneVector right_fill = PlaneVector(imgW);
      
      Plane visible = dummy;
      
      for(int x = imgW-1; x >= 0; --x)
      {
	if(!invalids[y * imgW + x + curview * imgW * imgH])
	  visible = getPlane(x,y);
	right_fill[x] = visible;
      }
      
      visible = dummy;
      
      for(int x = 0; x < imgW; ++x)
      {
	if(!invalids[y * imgW + x + curview * imgW * imgH])
	  visible = getPlane(x,y);
	
	float d_left = getD(x,y,visible);
	float d_right = getD(x,y,right_fill[x]);
	
	if(d_left < d_right)
	  getPlane(x,y) = visible;
	else
	  getPlane(x,y) = right_fill[x];
      }
    }
  }
  // bilateral median filter
  // not implemented for min_disp != 0
  if (min_disp != 0.f)
    return;
  
  int vec_size = ((int) max_disp + 1) * grayscale;
  PlaneVector planeHist = PlaneVector(vec_size + 1);
  for(int curview = 0; curview <= 1; ++curview)
  {
    changePointers(curview == 0);
    for(int y = 0; y < imgH; ++y)
    {
      for(int x = 0; x < imgW; ++x)
      {
	if(!invalids[y * imgW + x + curview * imgW * imgH])
	  continue;
	float sumweight = 0.f;
	FloatVector weightHist = FloatVector(vec_size,0.f);
	
	int start_x = x - windowsize / 2;
	int end_x   = x + windowsize / 2;
	int start_y = y - windowsize / 2;
	int end_y   = y + windowsize / 2;
	
	start_x = std::max(start_x, 0);
	end_x   = std::min(end_x, imgW);
	start_y = std::max(start_y, 0);
	end_y   = std::min(end_y, imgH);    
	
	cv::Vec3b colorp = cur_smooth->at<cv::Vec3b>(y,x);
	
	for(int wy = start_y; wy < end_y; ++wy)
	{
	  for(int wx = start_x; wx < end_x; ++wx)
	  {
	    Plane curplane = getPlane(wx,wy);
	    float d = getD(x,y,curplane);
	    
	    d = std::min(d,max_disp);
	    d = std::max(d,0.f);
	    
	    int index = (int) (d*grayscale);
	    float weight = colorweight(colorp,wx,wy);
	    sumweight += weight;
	    weightHist[index] += weight;
	    planeHist[index] = curplane;
	  }
	}
	float sum = 0.f;
	Plane curplane;
	for(int i = 0; i < vec_size; ++i)
	{
	  sum += weightHist[i];
	  curplane = planeHist[i];
	  
	  if(sum > sumweight / 2)
	    break;
	}
	getPlane(x,y) = curplane;
      }
    }
  }
}

void PatchMatch::pp_invalidatePixels(std::vector<int>& invalids)
{
  uint inv = 0;
  for(int curview = 0; curview <= 1; ++curview)
  {
    changePointers(curview == 0);
    for(int y = 0; y < imgH; ++y)
    {
      for(int x = 0; x < imgW; ++x)
      {
	Plane plane = getPlane(x,y);
	float d_ref = getD(x,y,plane);
	int match = x + prefix * (int) (d_ref + 0.5f);
	if(match < 0 || match >= imgW)
	{
	  invalids[y*imgW+x+curview*imgW*imgH] = 1; // set INVALID
	  ++inv;
	  continue;
	}
	int y_match = plane.y_disp +y;
	if(y_match < 0 || y_match >= imgH)
	  y_match = y;
	float d_match = getD(match,y_match,getPlane(match,y_match,false));
	if(abs(d_ref - d_match) > (float) lr_check_threshold)
	{
	  invalids[y*imgW+x+curview*imgW*imgH] = 1; // set INVALID
	  ++inv;
	}
      }
    }
  }
  stat->writeLine("Invalid points: " + std::to_string(inv));
}

void PatchMatch::dispMap(cv::OutputArray _dispL, cv::OutputArray _dispR, bool scaled)
{
  _dispL.create(imgH,imgW,CV_8UC1);
  _dispR.create(imgH,imgW,CV_8UC1);
  cv::Mat dispL = _dispL.getMat();
  cv::Mat dispR = _dispR.getMat();
  for(int view = 0; view <= 1; ++view)
  {
    changePointers(view == 0);
    for (int y = 0; y < imgH; ++y)
    {
      for (int x = 0; x < imgW; ++x)
      {
	int d_int;
	float d = getD(x, y,getPlane(x,y));
	if(scaled) 
	{   
	  d_int = (int) ((d - min_disp) * grayscale + 0.5f);
	  d_int = std::min(d_int, 255);
	  d_int = std::max(d_int, 0);
	} else {
	  d_int = (int) getD(x,y,getPlane(x,y));
	}
	(view == 0 ? dispL : dispR).at<uchar>(y,x) = (uchar) d_int;
      }
    }
  }
}

void PatchMatch::getDispMap(cv::OutputArray _dispL, cv::OutputArray _dispR, bool invert)
// plots disparity map
{ 
  cv::Mat testimg;
  if(solved)
  {
    dispMap(_dispL,_dispR,!invert);
  }
  else 
  {
    if(_dispL.empty())
    {
      _dispL.create(imgH,imgW,CV_8UC1);
      _dispR.create(imgH,imgW,CV_8UC1);
    }
    cv::Mat dispL = _dispL.getMat();
    cv::Mat dispR = _dispR.getMat();
    
    std::string file = output_dir + "disp" + output_ext + ".png";
    Helper::extractFrom7zFile(output_dir,file);
    
    cv::Mat both = cv::imread(file, CV_LOAD_IMAGE_GRAYSCALE);
    int next = both.cols / 2;
    if(invert)
      cv::bitwise_not(both,both);
    for(int y = 0; y < both.rows; ++y)
    {
      for(int x = 0; x < both.cols; ++x)
      {
	if(x >= next)
	  dispR.at<uchar>(y,x - next) = both.at<uchar>(y,x);
	else
	  dispL.at<uchar>(y,x) = both.at<uchar>(y,x);
      }
    }
    std::string cmd = "rm " + file;
    system(cmd.c_str());
  }
} 

void PatchMatch::writeDispMap()
// plots disparity map
{ 
  std::string output = output_dir + "disp" + output_ext + ".png";
  cv::Mat dispL,dispR,img;
  dispMap(dispL,dispR);
  Helper::makeTwoImagesOne(dispL,dispR,img);
  cv::imwrite(output,img);
  Helper::addTo7zFile(output_dir, output);
}

void PatchMatch::writePlane(std::ofstream& f, Plane& p)
{
  f << p.a << " " << p.b << " " << p.c << " " << p.z0 << " " << p.nx << " " << p.ny << " " << p.nz << " " << p.y_disp << std::endl;
}

void PatchMatch::writePlanes()
{
   std::string file = output_dir + "planes" + output_ext + ".dat";
   std::ofstream f(file.c_str());
   for(Plane p : left_planes)
     writePlane(f,p);
   for(Plane p : right_planes)
     writePlane(f,p);
   f.close();
   Helper::addTo7zFile(output_dir,file);
}

void PatchMatch::readPlanes()
{
   std::string file = output_dir + "planes" + output_ext + ".dat";
   Helper::extractFrom7zFile(output_dir,file);
   std::ifstream f(file.c_str());
   int i = 0;
   std::string line;
   Plane p;
   std::vector<std::string> tokens;
   int size = left_planes.size();
   for(int i = 0; i < size * 2; ++i)
   {
     std::getline(f,line);
     Helper::tokenize(line,tokens);
     std::stringstream(tokens[0]) >> p.a;
     std::stringstream(tokens[1]) >> p.b;
     std::stringstream(tokens[2]) >> p.c;
     std::stringstream(tokens[3]) >> p.z0;
     std::stringstream(tokens[4]) >> p.nx;
     std::stringstream(tokens[5]) >> p.ny;
     std::stringstream(tokens[6]) >> p.nz;
     std::stringstream(tokens[7]) >> p.y_disp;
     if(i > size)
       right_planes[i - size] = p;
     else
       left_planes[i] = p;
   }
   f.close();
   std::string cmd = "rm " + file;
   system(cmd.c_str());
}
