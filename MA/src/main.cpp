// Copyright (C) 2015 Dominik Wetzel <dominik.wetzel@fh-zwickau.de>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "patchmatch.h"
#include "rectify.h"
#include "statistic.h"
#include "types.h"
#include "helper.h"
#include <time.h>

#ifndef __WIN32__
  #include "reconstruction.h"
  std::string sep = "/";
#else
  std::string sep = "\\";
#endif

// number of iterations
int pm_iters = 3;
// size of window
int patch_w  = 31;
// adaptive support weight parameter for color (eq. (4))
float gamma_color = 10.f;
// adaptive support weight spatial term (not used)
float gamma_spatial = 0.f;
// cost for matching outside of the image
float bordercostspercentage = 1.85f;
// removal of isolated regions in postprocessing (0 turns it off)
int minregion = 0;
// maximum allowed disparity difference in lr check
int lr_check_threshold = 1;

// match measure parameters (see eq. 5)
float alpha = 0.9f;
float tau1 = 30.f;
float tau2 = 4.f;

// mindisp
float gmindisp = 0.f;
// maxdisp
float gmaxdisp = 16.f;
// scaling of disparity map
float gscale = 255.f / (gmaxdisp - gmindisp);

// sampling inside the window (leave this set to 1)
int sample_inc = 1;

int around = 0;
float around_cost = 0;
bool stat_override = false,
     stat_none = false,
     ocl = false,
     invert = false,
     useOuter = false;
int rectify = 0;
int matches = 0;
int reconstruction = 0;
int rec_value = 0;

char *gradleftfn, 
     *gradrightfn,
     *set,
     *type,
     *dir,
     *ref = "full",
     *stat_prefix = "";
bool maxdisp_set = false, mindisp_set = false, piece = false;
int piece_x = 0, piece_y = 0, piece_size = 0;
Refinement refinement = Refinement::FULL;
int cpu_count = 4;

std::map<std::string,float> options;

std::string smooth;
std::string input_dir = ".." + sep +".." + sep +"data" + sep;
std::string output_dir = ".." + sep +".." + sep + "results" + sep;
std::string output_ext = "";
std::string asift_path = ".." + sep + ".." + sep + "ASIFT" + sep + "build" + sep + "ASIFT_hornacek";

//////////////////////////////
//	command shell parsing	//
//////////////////////////////
void copystring (char *src, char *&dst)
{
	dst = (char*) malloc (512 * sizeof (char));

	std::size_t i;
	for (i = 0; i < strlen(src); i++)
		dst[i] = src[i];

	dst[i] = '\0';
}

void Usage()
{
	printf ("patchmatchstereo [-d,-s,-iters,-patch_w,-alpha,-tau1,-tau2,-gamma_color,-border_percentage,-minregion] left_img right_img output_img\n");

	exit(-1);
}

void process_commandline (int argc, char **argv)
{
	if (argc == 1) return;

	int pos = 1;
	
	int setscale = 0;
	
	bool use_dir = false;
	
	bool notype = true;

	while (1)
	{
		if (pos + 1 >= argc) break;

		if (strcmp (argv[pos], "-dmin") == 0)
		{
			if (!sscanf (argv[++pos], "%f", &gmindisp)) Usage();
			pos++;
			mindisp_set = true;
		}
		else
		if (strcmp (argv[pos], "-dmax") == 0)
		{
			if (!sscanf (argv[++pos], "%f", &gmaxdisp)) Usage();
			pos++;
			maxdisp_set = true;
		}
		else
		if (strcmp (argv[pos], "-setscale") == 0)
		{
			setscale = 1;
			pos++;
		}
		else
		if (strcmp (argv[pos], "-s") == 0)
		{
			if (!sscanf (argv[++pos], "%f", &gscale)) Usage();
			pos++;
		}
		else
		if (strcmp (argv[pos], "-iters") == 0)
		{
			if (!sscanf (argv[++pos], "%d", &pm_iters)) Usage();
			pos++;
		}
		else
		if (strcmp (argv[pos], "-patch_w") == 0)
		{
			if (!sscanf (argv[++pos], "%d", &patch_w)) Usage();
			pos++;
		}
		else
		if (strcmp (argv[pos], "-alpha") == 0)
		{
			if (!sscanf (argv[++pos], "%f", &alpha)) Usage();
			pos++;
		}
		else
		if (strcmp (argv[pos], "-tau1") == 0)
		{
			if (!sscanf (argv[++pos], "%f", &tau1)) Usage();
			pos++;
		}
		else
		if (strcmp (argv[pos], "-tau2") == 0)
		{
			if (!sscanf (argv[++pos], "%f", &tau2)) Usage();
			pos++;
		}
		else
		if (strcmp (argv[pos], "-gamma_color") == 0)
		{
			if (!sscanf (argv[++pos], "%f", &gamma_color)) Usage();
			pos++;
		}
		else
		if (strcmp (argv[pos], "-border_percentage") == 0)
		{
			if (!sscanf (argv[++pos], "%f", &bordercostspercentage)) Usage();
			pos++;
		}
		else
		if (strcmp (argv[pos], "-minregion") == 0)
		{
			if (!sscanf (argv[++pos], "%d", &minregion)) Usage();
			pos++;
		}
		else
		if (strcmp (argv[pos], "-sample_inc") == 0)
		{
			if (!sscanf (argv[++pos], "%d", &sample_inc)) Usage();
			pos++;
		}
		else
		if (strcmp (argv[pos], "-lr_check_threshold") == 0)
		{
			if (!sscanf (argv[++pos], "%d", &lr_check_threshold)) Usage();
			pos++;
		}
		else
		if (strcmp (argv[pos], "-type") == 0)
		{
			copystring(argv[++pos],type);
			notype = false;
			pos++;
		}
		else
		if (strcmp (argv[pos], "-a_path") == 0)
		{
			char* a_path;
			copystring(argv[++pos],a_path);
			asift_path = std::string(a_path);
			pos++;
		}
		if (strcmp (argv[pos], "-dir") == 0)
		{
			copystring(argv[++pos],dir);
			use_dir = true;
			pos++;
		}
		else
		if (strcmp (argv[pos], "-lines") == 0)
		{
			if (!sscanf (argv[++pos], "%d", &around)) Usage();
			pos++;
		}
		else
		if (strcmp (argv[pos], "-stat") == 0)
		{
			char* tmp;
			copystring(argv[++pos],tmp);
			if(strcmp(tmp,"no") == 0)
			  stat_none = true;
			else if(strcmp(tmp,"pref") == 0)
			  copystring(argv[++pos],stat_prefix);
			else if(strcmp(tmp,"or") == 0)
			  stat_override = true;
			++pos;
		}
		else
		if (strcmp (argv[pos], "-ocl") == 0)
		{
			ocl = true;
			++pos;
		}
		else
		if (strcmp (argv[pos], "-rectify") == 0)
		{
			++rectify;
			char* rect;
			copystring(argv[++pos],rect);
			if(strcmp(rect,"noOut") == 0) 
			{
			  ++rectify;
			  ++pos;
			} 
			else
			if(strcmp(rect,"withOut") == 0)
			{
			  useOuter = true;
			  ++rectify;
			  ++pos;
			}
			if(sscanf(argv[pos],"%d", &matches))
			{
			  ++pos;
			}
		}
		else
		if (strcmp (argv[pos], "-reconstruction") == 0)
		{
			++reconstruction;
			char* rec;
			copystring(argv[++pos],rec);
			if(strcmp(rec,"true") == 0) 
			{
			  ++reconstruction;
			  ++pos;
			} 
			else
			if(strcmp(rec,"inv") == 0)
			{
			  ++reconstruction;
			  invert = true;
			  ++pos;
			}
			if(sscanf(argv[pos], "%d", &rec_value))  
			  ++pos;
		}
		else
		if (strcmp (argv[pos], "-piece") == 0)
		{
			sscanf(argv[++pos], "%d", &piece_x);
			sscanf(argv[++pos], "%d", &piece_y);
			sscanf(argv[++pos], "%d", &piece_size);
			piece = true;
			++pos;
		}
		else
		if (strcmp (argv[pos], "-cpus") == 0)
		{
			sscanf(argv[++pos], "%d", &cpu_count);
			++pos;
		}
		else
		if (strcmp (argv[pos], "-ref") == 0)
		{
			copystring(argv[++pos],ref);
			if(strcmp(ref,"np") == 0)
			  refinement = Refinement::ROINP;
			else if(strcmp(ref,"fl") == 0)
			  refinement = Refinement::ROF;
			else if(strcmp(ref,"none") == 0)
			  refinement = Refinement::NONE;
			else{
			  refinement = Refinement::FULL;
			  copystring("full",ref);
			}
			++pos;
		}
		else
		if (strcmp (argv[pos], "-line_costs") == 0)
		{
			if (!sscanf (argv[++pos], "%f", &around_cost)) Usage();
			pos++;
		}
		else
			break;
	}

	if (setscale)
		gscale = 255.f / (gmaxdisp - gmindisp);
	if(notype)
	  type = "jpg";
	if(use_dir) {
	  input_dir += std::string(dir) + sep;
	  output_dir += std::string(dir) + sep;
	}
	if (pos + 1 != argc) { std::cout << argv[pos] << " " << argc << std::endl; Usage(); return;}

	copystring (argv[pos], set);
	//copystring (argv[pos++], rightfn);

// 	dispfn = (char*) malloc (512 * sizeof (char));
// 	sprintf (dispfn, "%s", argv[pos++]);

	printf ("PatchMatchStereo\n");
	printf ("gmindisp %.1f\n", gmindisp);
	printf ("gmaxdisp %.1f\n", gmaxdisp);
	printf ("gscale %.1f\n", gscale);
	printf ("pm_iters %d\n", pm_iters);
	printf ("patch_w %d\n", patch_w);
	printf ("alpha %.2f\n", alpha);
	printf ("tau1 %.2f\n", tau1);
	printf ("tau2 %.2f\n", tau2);
	printf ("gamma_color %.1f\n", gamma_color);
	printf ("border_percentage %.1f\n", bordercostspercentage);
	printf ("minregion %d\n", minregion);
	printf ("sample_inc %d\n", sample_inc);

	printf ("set %s\n", set);
	printf ("type %s\n", type);
	printf ("around %d\n", around);
}

//////////////////////////////
//	command shell parsing	//
//////////////////////////////
int main(int argc, char *argv[])
{
  if(argc == 1)
  {
    argc--;
    argv++;

    argc = 1;
    char *dummy[20];
    argv = dummy;

    argv[argc++] = "-cpus";
    argv[argc++] = "8";
    
//     argv[argc++] = "-iters";
//     argv[argc++] = "1";
    
//     argv[argc++] = "-ocl"; // Use OpenCL 

    argv[argc++] = "-stat";
    argv[argc++] = "no"; // No stats
//     argv[argc++] = "rest"; // rest Stats (extra file)

//     argv[argc++] = "-stat";    
//     argv[argc++] = "or"; // override stats
    
    argv[argc++] = "-rectify";
    argv[argc++] = "noOut";
//     argv[argc++] = "16";
    
    argv[argc++] = "-ref";
    argv[argc++] = "none";
//     argv[argc++] = "np"; // not propagated
//     argv[argc++] = "full"; // full
//     argv[argc++] = "fl"; // first line
    
//     argv[argc++] = "-reconstruction"; // Show Reconstruction
//     argv[argc++] = "true"; // Only Show Reconstruction
    
//     argv[argc++] = "-piece";
//     argv[argc++] = "63";
//     argv[argc++] = "110";
//     argv[argc++] = "256";

    argv[argc++] = "-lines";
    argv[argc++] = "5";
    
    argv[argc++] = "-line_costs";
    argv[argc++] = "0";
    
//     argv[argc++] = "-type";
//     argv[argc++] = "ppm";
//     argv[argc++] = "cones";
    
    argv[argc++] = "-dir";
    argv[argc++] = "own/small";
//     argv[argc++] = "blowball";
//     argv[argc++] = "butterfly";
//     argv[argc++] = "faculty";
    argv[argc++] = "bicycles";

//     argv[argc++] = "-dir";
//     argv[argc++] = "own/small/fail";    
//     argv[argc++] = "station";
//     argv[argc++] = "railroad";
//     argv[argc++] = "art";
//     argv[argc++] = "sign";

//     argv[argc++] = "-dir";
//     argv[argc++] = "holger/small";
// //     argv[argc++] = "car";
// //     argv[argc++] = "berg";
// //     argv[argc++] = "Geysir";
// //     argv[argc++] = "Glaumbaer";
// //     argv[argc++] = "Langhaus7";
//     argv[argc++] = "Tor";
// //     argv[argc++] = "Langhaus5";
// //     argv[argc++] = "ReykjavikOper1";
// //     argv[argc++] = "Selfoss3";
// //     argv[argc++] = "Hafen";

    gmindisp = 0.f;
    gmaxdisp = 64.f * 2;
    gscale = 255.f / (gmaxdisp - gmindisp);
    // Don't know why... but it is somehow needed initialization
    std::cout << "" << std::endl;
  }
  
  // read images
  process_commandline (argc, argv);

  input_dir = input_dir + set;
  std::string leftfn = input_dir + "_left." + type;
  std::string rightfn = input_dir + "_right." + type;

  cv::Mat left = cv::imread (leftfn);
  cv::Mat right = cv::imread (rightfn);
  
  if(left.size().width == 0 || right.size().width == 0) {
    std::cout << "ERROR: Couldn't load image/s" << std::endl;
    return 0;
  }

  output_ext =  std::string(rectify < 1 ? "_unrect" : "") + 
		"_l" + std::to_string(around) +
		std::string(around_cost != 0 ? "_c" + std::to_string(around_cost) : "") +
		"_r" + std::string(ref);
  
  output_dir = output_dir + set + sep;
  std::string rect_out = output_dir + "rectify" +sep;
  smooth = output_dir + set + "_smooth";
  system(std::string("mkdir -p " + output_dir).c_str());
  system(std::string("mkdir -p " + rect_out).c_str());
  if(piece) system(std::string("mkdir -p " + output_dir + "piece" + sep).c_str());

//   std::string d_out = output_dir + "disp" + output_ext + ".png";
//   Helper::extractFrom7zFile(output_dir,d_out);
//   std::ifstream f(d_out);
//   bool good = f.good();
//   system(std::string("rm " + d_out).c_str());
//   if(good)
//     return 0;
  
  std::cout << "Do rectification" << std::endl;
  Statistic* statPtr = NULL;
  if(!stat_none)
    statPtr = new Statistic(output_dir,std::string(piece ? "piece" + sep : "") + set + output_ext,around,stat_override,std::string(stat_prefix));
  
  cv::Mat rect_l = left.clone(), rect_r = right.clone();
  
  cv::Mat F,K;
  std::vector<cv::Point2f> pointMatch(4);
  
  // Rectification
  {
    Rectification rect = Rectification(left, right, rect_out,asift_path, matches > 0, NULL, matches);
    F = rect.rectify(HARTLEY,F_QER,rectify > 0);
    K = rect.getK();
    rect.getOneMatch(pointMatch);
    
    std::pair<float,float> dispRange = rect.getDisprange();
    float range = dispRange.second - dispRange.first;
//     if(dispRange.second < 0)
//     {
//       cv::Mat tmp = left.clone();
//       left = right.clone();
//       right = tmp.clone();
//     }
      
    if(!mindisp_set) 
    {
      gmindisp = 0;
    }
      
    if(!maxdisp_set)
    {
      //if(range < 255.f)
      {
	range += 100.f;
	if(range > 255.f)
	  range = 255.f;
      }
      gmaxdisp = range;
    }
      
    gscale = 255.f / (gmaxdisp - gmindisp);
    if(statPtr)
      statPtr->writeLine("min/max Disparity: " +  std::to_string(gmindisp) + " " + std::to_string(gmaxdisp));
  }
  if(rectify > 1)
  {
    Rectification rect = Rectification(rect_l, rect_r, rect_out,asift_path, matches > 0, statPtr,matches,useOuter);

    rect.rectify(HARTLEY,F_8POINT);
    rect.rectify(POLLEFEY, F_8POINT);
  
    rect.rectify(HARTLEY,F_QER);
    rect.rectify(POLLEFEY, F_QER);
  
    rect.rectify(BOUGET, F_AUTO); 
    rect.rectify(HARTLEY,F_CALIB_5POINT);
    rect.rectify(POLLEFEY, F_CALIB_5POINT);
    rect.rectify(QER,F_AUTO);
    
    rect.compareYStretch(QER, HARTLEY, F_QER);
  }

  if(piece) 
  {
    cv::Mat tmpl = cv::Mat(piece_size,piece_size,left.type());
    cv::Mat tmpr = cv::Mat(piece_size,piece_size,right.type());
    for(int i = 0; i < piece_size; ++i)
    {
      for(int j = 0; j < piece_size; ++j)
      {
	tmpl.at<cv::Vec3b>(i,j) = left.at<cv::Vec3b>(i + piece_y,j + piece_x);
	tmpr.at<cv::Vec3b>(i,j) = right.at<cv::Vec3b>(i + piece_y,j + piece_x);
      }
    }
    left = tmpl.clone();
    right = tmpr.clone();
    cv::imwrite(output_dir + "piece" + sep + "piece_left" + output_ext + ".png", left);
    cv::imwrite(output_dir + "piece" + sep + "piece_right" + output_ext + ".png", right);
  }

  options["refinment"] = (float) refinement;
  options["max_disp"] = gmaxdisp;
  options["min_disp"] = gmindisp;
  options["around"] = around;
  options["cpus"] = cpu_count;
  options["iters"] = pm_iters;
  options["around_cost"] = around_cost;
  
  cv::destroyAllWindows();
  
  // PatchMatch
  cv::Mat dispMapL, dispMapR;
  {
    time_t start, end;
    std::time(&start);
    // run patchmatch
    PatchMatch pm = PatchMatch(left,right,options,statPtr);
    if(reconstruction < 2 && rectify < 2)
      pm.solve(ocl,strcmp(stat_prefix,"") != 0);
    std::time(&end);
    double duration = std::difftime(end,start);
    if(statPtr)
      statPtr->writeLine("Time for Patchmatch: " + std::to_string(duration) + "s");
    
    pm.getDispMap(dispMapL,dispMapR,invert);
  }
  
#ifndef __WIN32__
  // 3D Reconstruction
  if(reconstruction > 0)
  {    
    std::cout << "Start Reconstruction" << (invert ? " ---INVERT---" : "") << std::endl;
    Reconstruction r = Reconstruction(left,right,F,K,pointMatch,rectify > 0);
    if(rectify < 2)
    {
      float d_range = rec_value > 0 ? rec_value : gmaxdisp - gmindisp;
      d_range = invert ? 10*d_range : d_range;
      r.solve(d_range, dispMapL, dispMapR);
    }
  }
#endif
  
  delete statPtr;
  
  return 0;
}
