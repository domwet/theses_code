// Copyright (C) 2015 Dominik Wetzel <dominik.wetzel@fh-zwickau.de>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "open_cl_stuff.h"
#include <iostream>
#include <fstream>
#include <exception>
#include "types.h"
#include "opencv2/opencv.hpp"

bool OCL::checkErr(cl_int err, std::string s)
{
  if(err != CL_SUCCESS)
  {
    if(s != "false")
      std::cout << "ERROR: " << err << " " << s << std::endl;
    return false;
  }
  return true;
}

void OCL::prepareImage(cv::Mat& src, std::vector<uint>& dst)
{
  dst = std::vector<cl_uint>(src.size().area() * 4);
  for(int y = 0; y < src.rows; ++y)
  {
    for(int x = 0; x < src.cols; ++x)
    {
      size_t idx = (y * src.cols + x) * 4;
      cv::Vec4b vec = src.at<cv::Vec4b>(y,x);
      dst[idx + 0] = (cl_uint) vec[0];
      dst[idx + 1] = (cl_uint) vec[1];
      dst[idx + 2] = (cl_uint) vec[2];
      dst[idx + 3] = (cl_uint) vec[3];
    }
  }
}

void OCL::initBuffers(cv::Mat *cur, cv::Mat *other, 
		      cv::Mat *curSmooth, 
		      PlaneVector* planes,
		      FloatVector* costs,
		      FloatVector& exps_color,
		      FloatVector& addData,
		      PlaneVector* viewNeigbors,
		      std::vector<int>* viewCount
)
{
  additionalData = addData;
  try
  {
  cv::Mat smooth;
  cv::cvtColor(*curSmooth,smooth,CV_BGR2BGRA);
  prepareImage(*cur,cImg);
  prepareImage(*other,oImg);
  prepareImage(smooth,sImg);
  
  size_t imgW = cur->cols;
  size_t imgH = cur->rows;
  
  //initImages
  cl::ImageFormat formatRGB(CL_RGBA,CL_UNSIGNED_INT32);
  smoothImg = cl::Image2D(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, formatRGB, imgW,imgH,0,&(sImg[0]));
  curImg = cl::Image2D(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, formatRGB, imgW,imgH,0,&(cImg[0]));
  otherImg = cl::Image2D(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, formatRGB, imgW,imgH,0,&(oImg[0]));
  
  costBuffer = cl::Buffer(context, CL_MEM_READ_WRITE |
    CL_MEM_COPY_HOST_PTR, costs->size() * sizeof(float), &(costs->at(0)), &err);
  planeBuffer = cl::Buffer(context, CL_MEM_READ_WRITE |
    CL_MEM_COPY_HOST_PTR, planes->size() * sizeof(cl_float8), &(planes->at(0)), &err);
  
  if(viewNeigbors)
  {
    neighborBuffer = cl::Buffer(context, CL_MEM_READ_ONLY |
    CL_MEM_COPY_HOST_PTR, viewNeigbors->size() * sizeof(cl_float8), &(viewNeigbors->at(0)), &err);
    countBuffer = cl::Buffer(context, CL_MEM_READ_ONLY |
    CL_MEM_COPY_HOST_PTR, viewCount->size() * sizeof(int), &(viewCount->at(0)), &err);
  }
    
  expsBuffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, exps_color.size() * sizeof(float), &(exps_color[0]),&err);
  
  size_t size = imgW * imgH * additionalData[11] * 4;  
  rands = std::vector<float>(size);
  for(int i = 0; i < size; ++i)
  {
    rands[i] = rand();
  }
  
  randBuffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, (size / 4) * sizeof(cl_float4),&(rands[0]), &err);  
  }   
  catch(cl::Error e)
  {
    std::cout << e.what() << "(" << e.err() << ")" << std::endl;
    throw e;
  }
}

void OCL::runKernel(bool init, Refinement ref, int iter, int x, int y)
{
  size_t imgW = additionalData[5];
  size_t imgH = additionalData[6];
  try{
  if(!init)
  { 
    int type = 0;
    if(x < 0)
      x = 0;
    else
    {
      imgW = 1;
      type = 1;
    }
    
    if(y < 0)
      y = 0;
    else
    {
      imgH = 1;
      type = type == 1 ? 3 : 2;
    }
    
    elem_size = MAXNEIGHBOR * additionalData[7] + 2;
    g_size = imgW * imgH * elem_size;    
    gather = FloatVector(g_size * 16,0.f);
    gatherBuffer = cl::Buffer(context,CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
			g_size * sizeof(cl_float16),(void*) &(gather[0]));
    
    cl::Kernel progKernel = gatherKernel;
    
    progKernel.setArg(0,sizeof(cl_mem), (void*) &planeBuffer);
    progKernel.setArg(1,sizeof(cl_mem), (void*) &countBuffer);
    progKernel.setArg(2,sizeof(cl_mem), (void*) &neighborBuffer);
    progKernel.setArg(3,sizeof(cl_mem), (void*) &gatherBuffer);
    progKernel.setArg(4,sizeof(cl_uint), (void*) &elem_size);
    progKernel.setArg(5,sizeof(cl_float16),(void*) &(additionalData[0]));
    progKernel.setArg(6,sizeof(cl_int),(void*) &x);
    progKernel.setArg(7,sizeof(cl_int),(void*) &y);
    progKernel.setArg(8,sizeof(cl_int),(void*) &type);
    
    queue.enqueueNDRangeKernel(progKernel,cl::NullRange,cl::NDRange(imgW,imgH),cl::NullRange); 
    
    progKernel = matchCostsKernel;
    
    progKernel.setArg(0,sizeof(cl_mem), (void*) &curImg);
    progKernel.setArg(1,sizeof(cl_mem), (void*) &otherImg);
    progKernel.setArg(2,sizeof(cl_mem), (void*) &smoothImg);
    progKernel.setArg(3,sizeof(cl_mem), (void*) &gatherBuffer);
    progKernel.setArg(4,sizeof(cl_mem), (void*) &expsBuffer);
    progKernel.setArg(5,sizeof(cl_float16), (void*) &(additionalData[0]));
    queue.enqueueNDRangeKernel(progKernel,cl::NullRange,cl::NDRange(g_size),cl::NullRange); 
    
    progKernel = compareKernel;
    
    progKernel.setArg(0,sizeof(cl_mem), (void*) &planeBuffer);
    progKernel.setArg(1,sizeof(cl_mem), (void*) &costBuffer);
    progKernel.setArg(2,sizeof(cl_mem), (void*) &gatherBuffer);
    progKernel.setArg(3,sizeof(cl_uint), (void*) &elem_size);
    progKernel.setArg(4,sizeof(cl_float16),(void*) &(additionalData[0]));
    progKernel.setArg(5,sizeof(cl_int),(void*) &x);
    progKernel.setArg(6,sizeof(cl_int),(void*) &y);
    progKernel.setArg(7,sizeof(cl_int),(void*) &type);
    
    queue.enqueueNDRangeKernel(progKernel,cl::NullRange,cl::NDRange(imgW,imgH),cl::NullRange); 
    
    if(ref == Refinement::FULL ||
      (ref == Refinement::ROF && iter == 0))
    {
      progKernel = refinementKernel;
      
      progKernel.setArg(0,sizeof(cl_mem), (void*) &curImg);
      progKernel.setArg(1,sizeof(cl_mem), (void*) &otherImg);
      progKernel.setArg(2,sizeof(cl_mem), (void*) &smoothImg);
      progKernel.setArg(3,sizeof(cl_mem), (void*) &planeBuffer);
      progKernel.setArg(4,sizeof(cl_mem), (void*) &costBuffer);
      progKernel.setArg(5,sizeof(cl_mem), (void*) &expsBuffer);
      progKernel.setArg(6,sizeof(cl_mem), (void*) &randBuffer);
      progKernel.setArg(7,sizeof(cl_float16), (void*) &(additionalData[0])); 
      progKernel.setArg(11,sizeof(cl_int),(void*) &x);
      progKernel.setArg(12,sizeof(cl_int),(void*) &y);

      float max_delta_z = additionalData[3] + 1.f;
      float max_delta_angle = 1.f;
      int i = 0;
      while(max_delta_z > 0.1f)
      {
	progKernel.setArg(8,sizeof(cl_float),(void*) &max_delta_z);
	progKernel.setArg(9,sizeof(cl_float),(void*) &max_delta_angle);
	progKernel.setArg(10,sizeof(cl_float),(void*) &i);
	queue.enqueueNDRangeKernel(progKernel,cl::NullRange,cl::NDRange(imgW,imgH),cl::NullRange);
	
	max_delta_z /= 2.f;
	max_delta_angle /= 2.f;
	++i;
      }
    }
  }
  else
  {
    cl::Kernel kernel = initKernel;
    kernel.setArg(0,sizeof(cl_mem), (void*) &curImg);
    kernel.setArg(1,sizeof(cl_mem), (void*) &otherImg);
    kernel.setArg(2,sizeof(cl_mem), (void*) &smoothImg);
    kernel.setArg(3,sizeof(cl_mem), (void*) &planeBuffer);
    kernel.setArg(4,sizeof(cl_mem), (void*) &costBuffer);
    kernel.setArg(5,sizeof(cl_mem), (void*) &expsBuffer);
    kernel.setArg(6,sizeof(cl_mem), (void*) &randBuffer);
    kernel.setArg(7,sizeof(cl_float16), (void*) &(additionalData[0])); 
    queue.enqueueNDRangeKernel(kernel,cl::NullRange,cl::NDRange(imgW,imgH),cl::NullRange);
  }
  queue.enqueueBarrier();
  }
  catch(cl::Error e)
  {
    std::cout << e.what() << "(" << e.err() << ")" << std::endl;
    throw e;
  } 
}

void OCL::readBuffers(PlaneVector* cur_planes, FloatVector* cur_costs)
{
  queue.enqueueReadBuffer(planeBuffer,CL_TRUE,0,cur_planes->size() * sizeof(cl_float8),(void*) &(cur_planes->at(0)));
  queue.enqueueReadBuffer(costBuffer,CL_TRUE,0,cur_costs->size() * sizeof(float),(void*) &(cur_costs->at(0)));
  queue.finish();  
}


OCL::OCL()
{
  cl_int err;
  std::vector<cl::Platform> platformList;
  cl::Platform::get(&platformList);
  std::string name;
  bool contextSet = false;
  cl::Platform p;
  
  // try GPU
  for(cl_uint i = 0; i < platformList.size(); ++i)
  {
    p = platformList[i];
    checkErr(p.getInfo((cl_platform_info) CL_PLATFORM_NAME, &name));
    cl_context_properties cprops[3] = {CL_CONTEXT_PLATFORM, (cl_context_properties)(p)(), 0};
    try
    {
      context = cl::Context(CL_DEVICE_TYPE_GPU,cprops,NULL,NULL,&err);
      std::cout << "Context set with " << name << " as GPU device" << std::endl;
      contextSet = true;
      break;
    } 
    catch (cl::Error e) {}
  }
  
  // try CPU
  if(!contextSet)
  {
    for(cl_uint i = 0; i < platformList.size(); ++i)
    {
      p = platformList[i];
      p.getInfo((cl_platform_info) CL_PLATFORM_NAME, &name);
      std::cout << name << std::endl;
      cl_context_properties cprops[3] = {CL_CONTEXT_PLATFORM, (cl_context_properties)(p)(), 0};
      try
      {
	context = cl::Context(CL_DEVICE_TYPE_CPU,cprops,NULL,NULL,&err);
	std::cout << "Context set with " << name << " as CPU device" << std::endl;
	contextSet = true;
	break;
      } 
      catch (cl::Error e) {
	std::cout << e.what() << "(" << e.err() << ")" << std::endl;
      }
    }
  }
  
  if(!contextSet)
  {
    std::cout << "No Suitable device available: Aborting" << std::endl;
    exit(0);
  }
  
  std::vector<cl::Device> devices = context.getInfo<CL_CONTEXT_DEVICES>(&err);

  queue = cl::CommandQueue(context,devices[0]);
  
  std::string program_string;
  std::ifstream in("../Kernel/PatchMatch.cl", std::ios::in | std::ios::binary);
  if (in)
  {
    in.seekg(0, std::ios::end);
    program_string.resize(in.tellg());
    in.seekg(0, std::ios::beg);
    in.read(&program_string[0], program_string.size());
    in.close();
  }
  else
  {
    std::cout << "ERROR could not read file" << std::endl;
  }
  
  cl::Program::Sources source(1,std::make_pair(program_string.c_str(),program_string.size()));
  cl::Program prog(context, source, &err);
  
  char* options = NULL;
  try
  {
    prog.build(devices,options);
    initKernel = cl::Kernel(prog,"initialize");
    gatherKernel = cl::Kernel(prog,"gatherPlanes");
    refinementKernel = cl::Kernel(prog, "refinement");
    matchCostsKernel = cl::Kernel(prog, "matchCostCalc");
    compareKernel = cl::Kernel(prog,"compare");
  }
  catch (cl::Error e)
  {
    std::cout << "BuildLog:\n" << prog.getBuildInfo<CL_PROGRAM_BUILD_LOG>(devices[0]) << std::endl;
    throw e;
  }
  std::cout << "Init done" << std::endl;
}
