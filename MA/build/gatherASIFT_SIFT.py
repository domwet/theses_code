#!/usr/bin/env python
# Copyright (c) 2015 Dominik Wetzel <dominik.wetzel@fh-zwickau.de>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

import sys
import os
import os.path
import collections
build_dir = "build"

# relativ zum Ausführungsort... absolute Pfade gehen nicht
if len(sys.argv) < 2:
    print("Pfad zum Ausführungsort [Prefix]")
    exit()
  
directory = sys.argv[1]

dirlist = os.listdir(directory)
dirlist.sort()

dirlist = dirlist[1:]

dictionary = {}
prefix = ""
if len(sys.argv) > 2:
    prefix = sys.argv[2]

j = 0
for dir0 in dirlist:
    try:
        statFile = open(directory + dir0 + "/" + prefix + "rest_stats.txt", "r")
    except FileNotFoundError:
        continue

    ASIFT_data = []
    SIFT_data = []
    
    for line in statFile:
        if line.find("ASIFT:") >= 0:
            ASIFT_data = line[line.index(":")+1:].split("\t")[1:]
            ASIFT_data = [x.strip() for x in ASIFT_data]
        if line.find("SIFT:") >= 0:
            SIFT_data = line[line.index(":")+1:].split("\t")[1:]
            SIFT_data = [x.strip() for x in SIFT_data]
    dictionary[dir0] = [ASIFT_data,SIFT_data]
    statFile.close()

out = "./gatheredASIFT_SIFT.csv"
outfile = open(out,"w")

header = ["TestSet","Method","outlier removed points", "all points","time in s"]

for h in header:
    if h == header[len(header) - 1]:
        outfile.write(h + "\n")
    else:
        outfile.write(h + ";")

for dir0 in dictionary:
    list0 = dictionary[dir0]
    
    ASIFT = True
    for li in list0:
        if ASIFT:
            outfile.write(dir0 + ";ASIFT")
        else:
            outfile.write(dir0 + ";SIFT")
        for l in li:
            if l.find(".") >= 0:
                l = l[:l.index(".")]
            outfile.write(";" + l)
        ASIFT = False
        outfile.write("\n")
outfile.close()
print("Wrote file " + os.path.abspath(out))
