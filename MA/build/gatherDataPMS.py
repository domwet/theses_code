#!/usr/bin/env python
# Copyright (C) 2015 Dominik Wetzel <dominik.wetzel@fh-zwickau.de>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

import sys
import os
import os.path
import collections
build_dir = "build"

# relativ zum Ausführungsort... absolute Pfade gehen nicht
if len(sys.argv) < 2:
    print("Pfad zum Ausführungsort [Prefix]")
    exit()
  
directory = sys.argv[1]

dirlist = os.listdir(directory)
dirlist.sort()

dirlist = dirlist[1:]

dictionary = {}
prefix = ""
if len(sys.argv) > 2:
    prefix = sys.argv[2]

j = 0
for dir0 in dirlist:
    try:
        statFile = open(directory + dir0 + "/" + prefix + "stats.txt", "r")
    except FileNotFoundError:
        continue

    curAround = 0
    curRef = "full"
    curRect = True
    
    avg0 = 0
    min0 = 0
    max0 = 0
    median0 = 0
    time = 0
    invalids = 0
    line_cost = 0
    
    dict0 = collections.OrderedDict()
    list0 = []
    hist = []
    
    it = -1

    for line in statFile:
        if line.find("Stats for") >= 0:
            if line.find("_unrect") >= 0:
               curRect = False
            else:
               curRect = True
            pos1 = line.index("_l")
            pos15 = line.find("_c")
            pos2 = line.index("_r")
            pos3 = line.index(" at ")
            
            if(pos15 >= 0):
                curAround = int(line[pos1+2:pos15])
                line_cost = float(line[pos15+2:pos2])
            else:
                curAround = int(line[pos1+2:pos2])
                line_cost = 0
            curRef = line[pos2+2:pos3]
        elif line.find("Time for") >=0:
            time = int(float(line[line.index(":")+1:line.index("s")]))
            it = -1
            list0.append((curRect,curAround,line_cost,time,invalids,hist,dict0))
            hist = []
            dict0 = collections.OrderedDict()
        elif line.find("average") >= 0:
            pos1 = line.index("matchcost:") + 10
            pos2 = line.index("min",pos1)
            pos21 = line.index("[",pos2)
            pos22 = line.index("|",pos21)
            pos23 = line.index("]",pos22)
            pos3 = line.index("matchcost:",pos23) + 10
            
            avg0 = float(line[pos1:pos2].strip())
            min0 = float(line[pos21+1:pos22].strip())
            max0 = float(line[pos22+1:pos23].strip())
            median0 = float(line[pos3:].strip())
            dict0[it] = [avg0,min0,max0,median0]
            
        elif line.find("Iteration") >= 0:   
            it = int(line[10:].strip()[:-1])
        elif line.find("Histogram") >= 0:
            pos1 = line.index(":") + 1
            while(line.find("=",pos1) >= 0):
                pos1 = line.index("=",pos1)+1
                pos2 = line.index("\t",pos1)
                hist.append(line[pos1:pos2].strip())
        elif line.find("Invalid") >= 0:
            invalids = line[line.index(":")+1:].strip()
            
    dictionary[dir0] = list0
    statFile.close()

out = "./gatheredPMS.csv"
outfile = open(out,"w")

header = ["TestSet","aroundLines","rectified","avg","median","min","max","time","psi",
          "invalids","200","400","600","800","1000","1200","1400","1600","1800","2000",">2000"]

for h in header:
    if h == header[len(header) - 1]:
        outfile.write(h + "\n")
    else:
        outfile.write(h + ";")

for dir0 in dictionary:
    list0 = dictionary[dir0]
    for t in list0:
        dict0 = t[len(t)-1]
        hist = t[len(t)-2]
        (_,d) = dict0.popitem()
        list1 = [str(dir0) + ";",
                 str(t[1]) + ";",
                 str(t[0]) + ";",
                 str(d[0]) + ";",
                 str(d[3]) + ";",
                 str(d[1]) + ";",
                 str(d[2]) + ";",
                 str(t[3]) + ";",
                 str(t[2]) + ";",
                 str(t[4])]
        for h in hist:
            list1.append(";" + str(h))
        list1.append("\n")
        
        for o in list1:
            o = o.replace(".",",")
            outfile.write(o)
outfile.close()
print("Wrote file " + os.path.abspath(out))
