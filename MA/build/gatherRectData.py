#!/usr/bin/env python

import sys
import os
import os.path
build_dir = "build"

# relativ zum Ausführungsort... absolute Pfade gehen nicht
if len(sys.argv) < 2:
    print("Pfad zum Ausführungsort [prefix]")
    exit()
  
directory = sys.argv[1]

dirlist = os.listdir(directory)
dirlist.sort()

dirlist = dirlist[1:]

dictionary = {}

prefix = ""
if len(sys.argv) > 2:
    prefix = sys.argv[2]

# only once
header = []

j = 0
for dir0 in dirlist:
    try:
        statFile = open(directory + dir0 + "/" + prefix + "stats.txt", "r")
    except FileNotFoundError:
        continue
    lines = []
    for line in statFile:
        lines.append(line)
    statFile.close()
    
    lines.reverse()

    i = 0
    head = 0
    end = 0
    p_dev = 0
    y_dev = 0

    for line in lines:
        if line.find("Stats for") >= 0:
            break
        if line.find("Point deviation") >= 0:
            pos1 = line.index(":") + 1
            pos2 = line.index("Y",pos1)
            pos3 = line.index(":",pos2) + 1
            
            p_dev = float(line[pos1:pos2].strip())
            y_dev = float(line[pos3:].strip())
            
            end = i
        if line.find("ratio") >= 0:
            head = i
        i += 1

    if header == []:
        header.append("TestSet")
        tmp = lines[head].split("|")
        for l in tmp:
            header.append(l.strip())

    lines = lines[:i+1]
    lines.reverse()
  
    stats = lines[len(lines) + 1 - head:len(lines) - 1 - end]
    dict0 = {}
    for l in stats:
        tmp = l.split("|")
        list0 = []
        for s in tmp:
            list0.append(s.strip())
        dict0[list0[0]] = list0[1:]
    dictionary[dir0] = (dict0,p_dev,y_dev)
  
out = "./gathered.csv"
outfile = open(out,"w")

size = len(header) - 1

for h in header:
    if h == header[size]:
        outfile.write(h + "\n")
    else:
        outfile.write(h + ";")
        
for dt in dictionary:
    t = dictionary[dt]
    dict0 = t[0]
    for method in dict0:
        outfile.write(dt + ";" + method + ";")
        list0 = dict0[method]
        i = 1
        for data in list0:
            i += 1
            data = data.replace(".",",")
            if i == size:
                outfile.write(data)
                if method.find("qer") == 0:
                    outfile.write(";" + str(t[1]).replace(".",",") + ";" + str(t[2]).replace(".",","))
                outfile.write("\n")
            else:
                outfile.write(data + ";")
outfile.close()
print("Wrote file " + os.path.abspath(out))
