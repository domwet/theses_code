# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/src/clickimage.cpp" "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/build/CMakeFiles/patchmatchstereo.dir/src/clickimage.o"
  "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/src/ext/calibration_finder.cc" "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/build/CMakeFiles/patchmatchstereo.dir/src/ext/calibration_finder.o"
  "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/src/ext/five_point_pose.cc" "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/build/CMakeFiles/patchmatchstereo.dir/src/ext/five_point_pose.o"
  "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/src/ext/hornacek_motion.cc" "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/build/CMakeFiles/patchmatchstereo.dir/src/ext/hornacek_motion.o"
  "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/src/ext/pollefey/polarcalibration.cpp" "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/build/CMakeFiles/patchmatchstereo.dir/src/ext/pollefey/polarcalibration.o"
  "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/src/ext/pollefey/poll_rect.cpp" "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/build/CMakeFiles/patchmatchstereo.dir/src/ext/pollefey/poll_rect.o"
  "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/src/ext/pollefey/visualizePolarCalibration.cpp" "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/build/CMakeFiles/patchmatchstereo.dir/src/ext/pollefey/visualizePolarCalibration.o"
  "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/src/ext/qer/qer_homography.cpp" "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/build/CMakeFiles/patchmatchstereo.dir/src/ext/qer/qer_homography.o"
  "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/src/ext/qer/qer_match.cpp" "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/build/CMakeFiles/patchmatchstereo.dir/src/ext/qer/qer_match.o"
  "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/src/ext/qer/qer_matrix.cpp" "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/build/CMakeFiles/patchmatchstereo.dir/src/ext/qer/qer_matrix.o"
  "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/src/ext/qer/qer_numerics.cpp" "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/build/CMakeFiles/patchmatchstereo.dir/src/ext/qer/qer_numerics.o"
  "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/src/ext/qer/qer_rect.cpp" "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/build/CMakeFiles/patchmatchstereo.dir/src/ext/qer/qer_rect.o"
  "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/src/ext/qer/qer_vector.cpp" "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/build/CMakeFiles/patchmatchstereo.dir/src/ext/qer/qer_vector.o"
  "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/src/main.cpp" "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/build/CMakeFiles/patchmatchstereo.dir/src/main.o"
  "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/src/open_cl_stuff.cpp" "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/build/CMakeFiles/patchmatchstereo.dir/src/open_cl_stuff.o"
  "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/src/patchmatch.cpp" "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/build/CMakeFiles/patchmatchstereo.dir/src/patchmatch.o"
  "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/src/rectify.cpp" "/home/dom/Dokumente/FH-Zwickau/Masterthesis/Projects/PatchMatchStereo/build/CMakeFiles/patchmatchstereo.dir/src/rectify.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "DISABLE_OPENNI"
  "DISABLE_OPENNI2"
  "DISABLE_PCAP"
  "DISABLE_PNG"
  "EIGEN_USE_NEW_STDVECTOR"
  "EIGEN_YES_I_KNOW_SPARSE_MODULE_IS_NOT_STABLE_YET"
  "vtkFiltersFlowPaths_AUTOINIT=1(vtkFiltersParallelFlowPaths)"
  "vtkIOExodus_AUTOINIT=1(vtkIOParallelExodus)"
  "vtkIOGeometry_AUTOINIT=1(vtkIOMPIParallel)"
  "vtkIOImage_AUTOINIT=1(vtkIOMPIImage)"
  "vtkIOSQL_AUTOINIT=2(vtkIOMySQL,vtkIOPostgreSQL)"
  "vtkRenderingCore_AUTOINIT=4(vtkInteractionStyle,vtkRenderingFreeType,vtkRenderingFreeTypeOpenGL,vtkRenderingOpenGL)"
  "vtkRenderingFreeType_AUTOINIT=2(vtkRenderingFreeTypeFontConfig,vtkRenderingMatplotlib)"
  "vtkRenderingLIC_AUTOINIT=1(vtkRenderingParallelLIC)"
  "vtkRenderingVolume_AUTOINIT=1(vtkRenderingVolumeOpenGL)"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/opencv"
  "/usr/include/vtk-6.1"
  "/usr/include/freetype2"
  "/usr/include/jsoncpp"
  "/usr/include/python2.7"
  "/usr/include/libxml2"
  "/usr/include/pcl-1.7"
  "/usr/include/eigen3"
  "../include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
