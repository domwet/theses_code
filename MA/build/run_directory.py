#!/usr/bin/env python
# Copyright (C) 2015 Dominik Wetzel <dominik.wetzel@fh-zwickau.de>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

import sys
import os
build_dir = "build"

# relativ zum Ausführungsort... ganze Pfade gehen nicht
if len(sys.argv) < 2:
    print("Pfad relativ zum Ausführungsort")
    exit()
  
directory = sys.argv[1]
dir0 = os.path.relpath(directory, "../../data/")

dirlist = []
for f in os.listdir(directory):
    if f.find('.') >= 0:
	    dirlist.append(f)

dirlist.sort()
list0 = []
type0 = ""

i = 0#3
while i < len(dirlist) - 1:
    file1,type0 = dirlist[i].split('.')
    if type0 == "directory":
      i += 1
      continue
    file1 = file1[:file1.rindex('_')]

    i += 1

    file2,type0 = dirlist[i].split('.')
    file2 = file2[:file2.rindex('_')]

    if file1 == file2:
        list0.append(file1)
        i += 1;   

print(list0)

startset = input("Startset: ")

i = 0
notStart = True
rectified = " " #" -rectify"
stat = " -stat pref other_"
reconstruction = " " #" -reconstruction true"
parameters = [rectified + stat + reconstruction + " -stat or -line_costs 0 -lines 0 ",
              rectified + stat + reconstruction + " -line_costs 0 -lines 5 ",
              rectified + stat + reconstruction + " -line_costs 0 -lines 10 ",
              rectified + stat + reconstruction + " -line_costs 0.01 -lines 5 ",
              rectified + stat + reconstruction + " -line_costs 0.01 -lines 10 "]

#list0 = [startset]
#list0 = ["faculty","rock3","root"]
#list0 = ["blowball","bridge","butterfly","distel","dresden","flowers","maize","pole","rock1","rock2","stub","thing"]

for set0 in list0:
    if(set0 == startset) :
      notStart = False
    if(notStart) :
      continue
  
    str_i = str(i)
    
    if True:
      command = "./patchmatchstereo -type " + type0 + " -dir " + dir0 + " " + "-stat pref rest_ -rectify withOut -cpus 8" + " " + set0
      os.system(command)
      print("\n" + str_i + "# Kommand war: " + command + "\n")
      
      #command = "./patchmatchstereo -type " + type0 + " -dir " + dir0 + " -line_costs 0.01 -stat or -cpus 8 " + set0
      #os.system(command)
      #print("\n" + str_i + "# Kommand war: " + command + "\n")
      
      #command = "./patchmatchstereo -type " + type0 + " -dir " + dir0 + " -line_costs 0.01 -rectify -cpus 8 " + set0
      #os.system(command)
      #print("\n" + str_i + "# Kommand war: " + command + "\n")
      
      #command = "./patchmatchstereo -type " + type0 + " -dir " + dir0 + " -line_costs 0.01 -lines 5 -cpus 8 " + set0
      #os.system(command)
      #print("\n" + str_i + "# Kommand war: " + command + "\n")
      
      #command = "./patchmatchstereo -type " + type0 + " -dir " + dir0 + " -line_costs 0.01 -rectify -lines 5 -cpus 8 " + set0
      #os.system(command)
      #print("\n" + str_i + "# Kommand war: " + command + "\n")
      
      #command = "./patchmatchstereo -type " + type0 + " -dir " + dir0 + " -line_costs 0.01 -lines 10 -cpus 8 " + set0
      #os.system(command)
      #print("\n" + str_i + "# Kommand war: " + command + "\n")
    
      #command = "./patchmatchstereo -type " + type0 + " -dir " + dir0 + " -line_costs 0.01 -rectify -lines 10 -cpus 8 " + set0
      #os.system(command)
      #print("\n" + str_i + "# Kommand war: " + command + "\n")
    #else:
    
#for params in parameters:
  #for set0 in list0:
    #if(set0 == startset) :
      #notStart = False
    #if(notStart) :
      #continue
  
    #command = "./patchmatchstereo -type " + type0 + " -dir " + dir0 + " " + "-cpus 8" + params + set0
    #os.system(command)
    #print("Command was: " + command + "\n")
        
    ##command = "./patchmatchstereo -type " + type0 + " -dir " + dir0 + " -stat no -reconstruction inv" + params + set0
    ##os.system(command)
    ##print("Command was: " + command + "\n")

    
