// Copyright (C) 2015 Dominik Wetzel <dominik.wetzel@fh-zwickau.de>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef CLICK_IMAGE_H
#define CLICK_IMAGE_H

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>

class ClickImage {
  
private:
  float zoom;

  cv::Mat image;
  std::vector<cv::Point2f> *clicked;
  std::string outputDir;
  void fillImage(cv::InputOutputArray img);
  void writeImage(std::string file = "");
  
  static void mouseHandler(int event, int x, int y, int flags, void* param);
  static void mouseHandlerLines(int event, int x, int y, int flags, void* param);
  static void mouseHandlerRect(int event, int x, int y, int flags, void* param);

  
public:
  ClickImage(cv::InputArray img1, cv::InputArray img2, std::string outputDir);
  ~ClickImage(void);
  
  /**
   * writes copy of clicked points into pts
   */
  void getClicked(std::vector<cv::Point2f> &pts);
  
  /**
   * checks whether the old points should be used.
   */
  bool useOld();
  
  /**
   * Click some points and press Enter,
   * draws line between two points
   * if "file" is given image will be saved
   */
  int clickpoints(bool write = true, std::string file = "");
  
  void defineRectangle(std::vector<cv::Point2f>& rectangle, std::vector<cv::Point2f>& pts0,const std::string& package);
  
  /**
   * Saves the clicked points to a file with Format:\n
   * point.size\n
   * point1.x point1.y\n
   * point2.x point2.y\n
   * ...\n
   * pointn.x pointn.y
   */
  void savePoints(std::string file = "");
  
  /**
   * checks if given file already there and reads the points
   * returns amount of read points
   */
  int checkFile(bool write = true, std::string file = "");
  
  /**
   * click on a point in the image to compute the see the correspondent epilines for the given Fundamental-Matrix
   */
  void clickLines(cv::InputArray F);
};

#endif
