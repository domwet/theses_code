// Copyright (C) 2015 Dominik Wetzel <dominik.wetzel@fh-zwickau.de>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef OCL_STUFF
#define OCL_STUFF

#define __CL_ENABLE_EXCEPTIONS

#include <CL/cl.hpp>
#include <opencv2/opencv.hpp>
#include "types.h"


class OCL 
{
private:
  cl_int err;

  std::vector<cl_uint> sImg,cImg,oImg;
  std::vector<float> gather, additionalData;
  cl::Buffer costBuffer, planeBuffer, neighborBuffer, countBuffer, randBuffer, expsBuffer, gatherBuffer;
  cl::Image2D smoothImg, curImg, otherImg;
  std::vector<float> rands;
  cl::Context context;
  cl::Kernel initKernel, gatherKernel, matchCostsKernel, compareKernel, refinementKernel;
  cl::CommandQueue queue;
  int how = 0;

  size_t g_size;
  size_t elem_size;
  bool checkErr(cl_int err, std::string s = "");
  void prepareImage(cv::Mat& src, std::vector<uint>& dst);
  
public:
  OCL();
    
  void initBuffers(cv::Mat *cur, 
		 cv::Mat *other, 
		 cv::Mat *curSmooth, 
		 PlaneVector* planes,
		 FloatVector* costs,
		 FloatVector& exps_color,
		 FloatVector& additionalData,
		 PlaneVector* viewNeigbors = NULL,
		 std::vector<int>* viewCount = NULL
		);
  void runKernel(bool init,
		 Refinement ref = Refinement::FULL,
		 int iter = 0,
		 int x = -1,
		 int y = -1
		);
  
  void readBuffers(PlaneVector* cur_planes,
		  FloatVector* cur_costs);
};

#endif
