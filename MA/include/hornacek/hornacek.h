// Copyright (C) 2015 Dominik Wetzel <dominik.wetzel@fh-zwickau.de>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef HORNACEK_H
#define HORNACEK_H
#include <opencv2/core/core.hpp>

float find(cv::InputArray pts1_initial, cv::InputArray pts2_initial, cv::OutputArray Rs, cv::OutputArray ts, cv::OutputArray Es, cv::InputArray K, float inlier_thresh = 1, unsigned seed = 0);

#endif
