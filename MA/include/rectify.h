// Copyright (C) 2015 Dominik Wetzel <dominik.wetzel@fh-zwickau.de>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef _RECTIFY_H_
#define _RECTIFY_H_

#include <opencv2/opencv.hpp>
#include <string>
#include <fstream>
#include <sstream>
#include <opencv2/highgui/highgui.hpp>
#include "hornacek/hornacek.h"
#include "clickimage.h"
#include "statistic.h"
#include <ctime>

const std::string
  F_AUTO = "_auto",
  F_8POINT = "_8point",
  F_QER = "_qer",
  F_CALIB_5POINT = "_calib_5point";

const std::string 
  HARTLEY = "hartleys",
  POLLEFEY = "pollefey",
  QER = "qer",
  BOUGET = "bouget";  
  
class Rectification {
private:
  enum DistMethod{
    D1 = 1,
    D2 = 2,
    ONLY_Y = 3,
    D_INF = 8
  };
  
  std::string output_dir;
  std::string asift_path;
  
  const std::string FORMAT = "%-25s | %15s | %15s | %15s | %15s | %15s | %15s | %15s";
  bool firstRun = true;
  bool manual, useOuter;
  
  cv::Mat img1, img2, F, K, points1, points2, out_points1, out_points2;
  cv::Size imgSize;
  ClickImage* cimg;
  Statistic* stats;
  std::vector<cv::Point2f> rectangle;
  
  clock_t calculateF(std::vector<cv::Mat>& data, std::string algorithm = F_AUTO);
  clock_t rectification( const std::string algorithm, const std::string f_algorithm, std::vector<cv::Mat>& H);
  void rectifyImages(std::vector<cv::Mat> H, std::string prefix = "", bool show = true, bool override = false);
  std::string computeMatches(bool manual = false, unsigned n_matches = 0);
  void removeOutliers(float threshold = 2.5);
  void writePackage();
  
  float pointDistance(cv::Point2f p1, cv::Point2f p2, int method = DistMethod::D2);
  float avgEpipolarLineDistance(int img = 1, int method = DistMethod::D2);
  std::tuple<float,int> avgRectificationError(cv::InputArray points1, cv::InputArray points2, std::vector<cv::Mat>& maps);
  
  void removeShearing(cv::InputOutputArray homography);
  void getIntrinsicAndDistortion(cv::OutputArray K_, cv::OutputArray dist_, std::string file = "");
  void drawLines(cv::InputArray src, cv::OutputArray dst, std::vector<cv::Point2f> &points);
  void computeRectifiedPoints(std::vector<cv::Point2f> &points, cv::InputArray map, cv::InputArray mapy = cv::noArray());
  
  std::pair<float,float> disparity_range = std::pair<float,float>(0.0,0.0);

public:
  Rectification(cv::InputOutputArray img1, cv::InputOutputArray img2, std::string output_dir, std::string asift_path, bool manual = false, Statistic *stat = NULL, unsigned matches = 0, bool useOuter = false);
  inline ~Rectification() 
  {
    if(stats && stats->isDummy()) delete stats;
    delete cimg;
  }
  cv::Mat rectify(const std::string algorithm = HARTLEY, std::string f_algorithm = F_AUTO, bool override = false);
  void compareYStretch(const std::string algo1, const std::string algo2, const std::string f_algo);
  cv::Mat getK()
  {
    return K;
  };
  void getOneMatch(std::vector<cv::Point2f> &match);
  
  void clearF(void);
  inline std::pair<float,float> getDisprange() 
  {
    return disparity_range;
  }
};

#endif
