// Copyright (C) 2015 Dominik Wetzel <dominik.wetzel@fh-zwickau.de>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef _RECONSTRUCTION_H_
#define _RECONSTRUCTION_H_

#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/visualization/cloud_viewer.h>
#include <opencv2/opencv.hpp>

#include "types.h"
#include "helper.h"

extern std::string output_ext, output_dir;

class Reconstruction
{
private:
  typedef pcl::PointXYZRGB PCL_PointType;

  int width,height;
  cv::Mat imgL, imgR;
  cv::Mat F, Q, P1, P2;

  bool rectified;
  
  std::string out_left = output_dir + output_ext + "_left.pcd";
  std::string out_right = output_dir + output_ext + "_right.pcd";
  
  cv::Mat getProjectionMatrix(cv::InputArray _F, std::vector<cv::Vec3f>& lines, bool left)
  {
    cv::Mat F = _F.getMat();
    cv::Vec3f l1 = lines[0],l2 = lines[1];
    if(l1[0] == l2[0] && l1[1] == l2[1] && l1[2] && l2[2])
      l2 = lines[2];
    
    float lower = l1[0]*l2[1] - l2[0]*l1[1];
    float x = (left ? 1 : -1), y = 0, z = 0;
    
    if(lower > 0.0000001f)
    {
      x = (l1[1]*l2[2] - l2[1]*l1[2])/lower;
      y = (l2[0]*l1[2] - l1[0]*l2[2])/lower;
      z = 1;
    } 
    else 
    {
      F = cv::Mat::zeros(3,3,CV_64F);
      F.at<double>(2,1) = 1;
      F.at<double>(1,2) = -1;
      F = left ? F.t() : F;
    }
    
    cv::Mat e1 = cv::Mat(1,3,CV_64F);
    e1.at<double>(0) = x;
    e1.at<double>(1) = y;
    e1.at<double>(2) = z;
    
    cv::Mat ex = cv::Mat::zeros(3,3,CV_64F);
    ex.at<double>(0,1) = -z;
    ex.at<double>(1,0) = z;
    ex.at<double>(0,2) = y;
    ex.at<double>(2,0) = -y;
    ex.at<double>(1,2) = -x;
    ex.at<double>(2,1) = x;
    
    // Projective projection matrix = P = [ [e']x * F | e']
    
    cv::Mat P = (ex * F).t();
    P.push_back(e1);
    P = P.t();
    
    return P;
  }
  
  void getProjectionMatrices()
  {
    std::vector<cv::Point2f> initPts;
    std::vector<cv::Vec3f> linesL, linesR;
    initPts.push_back(cv::Point2f(20,50));
    initPts.push_back(cv::Point2f(30,40));
    initPts.push_back(cv::Point2f(20,40));
    
    Helper::computeEpilines(imgL.size(),F,linesL,linesR,initPts);
    
    P1 = getProjectionMatrix(F,linesR,false);
    P2 = getProjectionMatrix(F.t(),linesL,true);
  }
  
  void getRT(cv::InputArray _F, cv::InputArray _K, cv::Point2f pt1, cv::Point2f pt2, cv::OutputArray _R, cv::OutputArray _t, cv::InputArray P_ident)
  { 
    cv::Mat P_identity = P_ident.getMat();
    cv::Mat K = _K.getMat(), F = _F.getMat();    
    cv::Mat invK = K.inv();
    cv::Mat E = K.t() * F * K;
    
    // decompose E into the four possible projection matrices
    cv::vector<cv::Mat> P2 = getProjectionMatricesFromE(E);
    
    // find which of the four possible projection matrices is correct
    cv::vector<int> count(P2.size());
    cv::Mat obs1(3,1,CV_64F);
    obs1.at<double>(0,0) = pt1.x;
    obs1.at<double>(1,0) = pt1.y;
    obs1.at<double>(2,0) = 1.0;
    cv::Mat cam1 = invK * obs1;
    cv::Point2f x1(cam1.at<double>(0,0) / cam1.at<double>(2,0), cam1.at<double>(1,0) / cam1.at<double>(2,0));
    
    cv::Mat obs2(3,1,CV_64F);
    obs2.at<double>(0,0) = pt2.x;
    obs2.at<double>(1,0) = pt2.y;
    obs2.at<double>(2,0) = 1.0;
    cv::Mat cam2 = invK * obs2;
    cv::Point2f x2(cam2.at<double>(0,0) / cam2.at<double>(2,0), cam2.at<double>(1,0) / cam2.at<double>(2,0));
    
    unsigned int j = 0;
    while(j<P2.size())
    {
      cv::Point3d X = Triangulate(P_identity, P2[j], x1, x2);
      double P1_depth = X.z;
      double P2_depth = P2[j].at<double>(2,0) * X.x + P2[j].at<double>(2,1) * X.y + P2[j].at<double>(2,2) * X.z + P2[j].at<double>(2,3);
      if(P1_depth > 0.0 && P2_depth > 0.0){
	break;
      }
      ++j;
    }
    if(j == P2.size())
    {
      std::cout << "ERROR: No valid R and t found" << std::endl;
      return;
    }
    _R.getMatRef() = cv::Mat(P2[j], cv::Rect(0,0,3,3));
    _t.getMatRef() = cv::Mat(P2[j], cv::Rect(3,0,1,3));
  }
  
  void getProjectionMatrix(cv::InputArray _K, cv::Point2f& p_left, cv::Point2f& p_right)
  {
    cv::Mat R,t,T;
    cv::Mat K = _K.getMat();
    cv::Mat P_identity = cv::Mat::zeros(3,4,CV_64F);
    for(int j=0; j<3; ++j)
      P_identity.at<double>(j,j) = 1.0;
    
    P1 = K * P_identity;
    getRT(F,K,p_left,p_right,R,t,P_identity);
    T = t.t();
    P2 = R.t();
    P2.push_back(T);
    P2 = K * P2.t();
    
    if(rectified)
    {
      Q = cv::Mat::zeros(4,4,CV_64F);
      Q.at<double>(0,0) = 1;
      Q.at<double>(1,1) = 1;
      Q.at<double>(3,2) = 1/t.at<double>(0);
      Q.at<double>(0,3) = -K.at<double>(0,2);
      Q.at<double>(1,3) = -K.at<double>(1,2);
      Q.at<double>(2,3) = K.at<double>(0,0);
    }
  }
  
  cv::Point3d Triangulate(cv::Mat &P1, cv::Mat &P2, cv::Point2f &x1, cv::Point2f &x2)
  {
    cv::Mat A = cv::Mat::zeros(4,4,CV_64F);
    for(int i=0; i<4; ++i) 
    {
      A.at<double>(0,i) = x1.x * P1.at<double>(2,i) - P1.at<double>(0,i);
      A.at<double>(1,i) = x1.y * P1.at<double>(2,i) - P1.at<double>(1,i);
      A.at<double>(2,i) = x2.x * P2.at<double>(2,i) - P2.at<double>(0,i);
      A.at<double>(3,i) = x2.y * P2.at<double>(2,i) - P2.at<double>(1,i);
    }
    cv::Mat Xmat = cv::Mat::zeros(4,1,CV_64F);
    cv::SVD::solveZ(A, Xmat);
    cv::Point3d X;
    X.x = Xmat.at<double>(0,0) / Xmat.at<double>(3,0);
    X.y = Xmat.at<double>(1,0) / Xmat.at<double>(3,0);
    X.z = Xmat.at<double>(2,0) / Xmat.at<double>(3,0);
    
    return X;
  }
  
  std::vector<cv::Mat> getProjectionMatricesFromE(cv::InputArray _E)
  {
    cv::Mat E = _E.getMat();
    // follows the computation in
    // Recovering Baseline and Orientation from 'Esssential' Matrix
    // BKP Horn, Jan 1990
    cv::Mat ea(E, cv::Rect(0,0,1,3));
    cv::Mat eb(E, cv::Rect(1,0,1,3));
    cv::Mat ec(E, cv::Rect(2,0,1,3));
    
    // cofactor matrix (19), used both to find largest vector product and later for rotation
    cv::Mat cf(3,3,CV_64F);
    cv::Mat ebXec = eb.cross(ec);
    cv::Mat ecXea = ec.cross(ea);
    cv::Mat eaXeb = ea.cross(eb);
    
    for(int i=0; i<3; ++i)
    {
      cf.at<double>(0,i) = ebXec.at<double>(i,0);
      cf.at<double>(1,i) = ecXea.at<double>(i,0);
      cf.at<double>(2,i) = eaXeb.at<double>(i,0);
    }
    
    // find largest vector product
    cv::Mat cf2 = cf * cf.t();
    int max_index = 0;
    double max = 0.0;
    for(int i=0; i<3; ++i) 
    {
      if(cf2.at<double>(i,i) > max) 
      {
	max = cf2.at<double>(i,i);
	max_index = i;
      }
    }
    
    cv::Mat cfmax(cf, cv::Rect(0,max_index,3,1));
    cv::Mat cfmax2 = cfmax * cfmax.t();
    
    cv::Mat E2 = E * E.t();
    double trace = 0.0;
    for(int i=0; i<3; ++i)
      trace += E2.at<double>(i,i);
    
    cv::Mat t = cfmax.t();
    // calculate direction vector at proper length (18)
    t = t * 1.0/sqrt(cfmax2.at<double>(0,0)) * sqrt(0.5*trace);
    
    // scaling for rotation matrix later in (24)
    cv::Mat t2 = t.t()*t;
    const double s = 1/(t2.at<double>(0,0));
    
    cv::Mat tx = cv::Mat::zeros(3,3,CV_64F);
    tx.at<double>(0,1) = -t.at<double>(2,0);
    tx.at<double>(0,2) = t.at<double>(1,0);
    tx.at<double>(1,2) = -t.at<double>(0,0);
    tx.at<double>(1,0) = t.at<double>(2,0);
    tx.at<double>(2,0) = -t.at<double>(1,0);
    tx.at<double>(2,1) = t.at<double>(0,0);
    
    // the two rotation matrices using t and -t (24)
    cv::Mat Ra( s*(cf.t() - tx * E) );
    cv::Mat Rb( s*(cf.t() - (-1.0*tx) * E) );
    cv::Mat negt = -1.0 * t;
    
    // put all the solutions together
    cv::vector<cv::Mat> p(4);
    cv::Mat pp(3,4,CV_64F);
    cv::Mat ppR(pp, cv::Rect(0,0,3,3));
    cv::Mat ppt(pp, cv::Rect(3,0,1,3));
    
    Ra.copyTo(ppR);
    t.copyTo(ppt);
    pp.copyTo(p[0]);
    
    Ra.copyTo(ppR);
    negt.copyTo(ppt);
    pp.copyTo(p[1]);
    
    Rb.copyTo(ppR);
    t.copyTo(ppt);
    pp.copyTo(p[2]);
    
    Rb.copyTo(ppR);
    negt.copyTo(ppt);
    pp.copyTo(p[3]);
    
    return p;
  } 
  
  void showPCL(cv::InputArray input, bool left)
  {
    cv::Mat img = (left ? imgL : imgR);
    cv::Mat _3D = input.getMat();
     
    pcl::PointCloud<PCL_PointType>::Ptr cloud(new  pcl::PointCloud<PCL_PointType>);
    
    // Fill in the cloud data
    cloud->width    = width;
    cloud->height   = height;
    cloud->is_dense = true;
    cloud->resize(width * height);
    for(int y = 0; y < _3D.rows; ++y)
    {
      for(int x = 0; x < _3D.cols; ++x)
      {
	
	PCL_PointType p;
	cv::Vec3f pos = _3D.at<cv::Vec3f>(y,x);
	p.x = pos[0];
	p.y = pos[1];
	p.z = pos[2];
	
	cv::Vec3b bgr = img.at<cv::Vec3b>(y,x);
	
	p.r = bgr[2];
	p.g = bgr[1];
	p.b = bgr[0];
	
	// 	std::cout << x << bgr << std::endl;
	cloud->at(x,y) = p;
      }
    }
    pcl::visualization::CloudViewer viewer ("Simple Cloud Viewer");
    viewer.showCloud(cloud);
    while (!viewer.wasStopped ()){}
  }
  
  FloatVector removeInfAndGetBounds(cv::InputOutputArray _depthImage)
  {
    cv::Mat depth = _depthImage.getMat();
    cv::Mat norm = cv::Mat::zeros(depth.rows, depth.cols, depth.type());
    float maxX, minX, maxY, minY, maxZ, minZ;
    maxZ = maxY = maxX = -INFINITY;
    minZ = minY = minX = INFINITY;
    for(int i = 0; i < depth.size().area(); ++i)
    {
      cv::Vec3f pos = depth.at<cv::Vec3f>(i);
      bool inf = false;
      for(int j = 0; j < 3; ++j)
      {	
	if(pos[j] == INFINITY || pos[j] == -INFINITY)
	{
	  inf = true;
	  break;
	}
      }
      if(inf)
	continue;
      
      if(pos[0] > maxX) maxX = pos[0];
      if(pos[0] < minX) minX = pos[0];	
      if(pos[1] > maxY) maxY = pos[1];
      if(pos[1] < minY) minY = pos[1];	
      if(pos[2] > maxZ) maxZ = pos[2];
      if(pos[2] < minZ) minZ = pos[2];
      
      norm.at<cv::Vec3f>(i) = pos;
    }
    norm.copyTo(depth);
    
    FloatVector bounds;
    bounds.push_back(maxX);
    bounds.push_back(minX);
    bounds.push_back(maxY);
    bounds.push_back(minY);
    bounds.push_back(maxZ);
    bounds.push_back(minZ);
        
    return bounds;
  }
  
  void normalize(cv::InputOutputArray _depthImage, FloatVector& bounds)
  {
    cv::Mat depth = _depthImage.getMat();
    float maxX = bounds[0];
    float minX = bounds[1];
    float maxY = bounds[2];
    float minY = bounds[3];
    float maxZ = bounds[4];
    float minZ = bounds[5];
    
    for(int i = 0; i < depth.size().area(); ++i)
    {
      cv::Vec3f elem = depth.at<cv::Vec3f>(i);
      depth.at<cv::Vec3f>(i) = cv::Vec3f(((elem[0] - minX) / (maxX - minX)) * width,((elem[1] - minY) / (maxY - minY)) * height ,((elem[2] - minZ) / (maxZ - minZ)) *  1000);	
    }
  }
  
  void useQ(cv::InputArray disp, bool left)
  {
    cv::Mat depth;
    cv::reprojectImageTo3D(disp,depth,Q);
    FloatVector bounds = removeInfAndGetBounds(depth);
    showPCL(depth,left);
  }
  
  void useTriangulation(PlaneVector& planes, bool left = true)
  {
    cv::Mat depth = cv::Mat::zeros(height,width,CV_32FC3);
    for(int y = 0; y < height; ++y)
    {
      for(int x = 0; x < width; ++x)
      {
	Plane plane = planes.at(x + y * width);
	float d = plane.a * (float) x + plane.b * (float) y + plane.c;
	int match = x + (left ? -1 : 1) * (int) (d + 0.5f);
	if(match < 0 || match >= width)
	  continue;
	cv::Point2f p1 = cv::Point2f(x,y), p2 = cv::Point2f(match,y);
	cv::Point3f X = Triangulate(left ? P1 : P2, left ? P2 : P1,p1,p2);
	depth.at<cv::Vec3f>(y,x) = cv::Vec3f(X.x,X.y,X.z);
      }
    }
    FloatVector bounds = removeInfAndGetBounds(depth);
    showPCL(depth,left);
  }
  
  void printPCL(bool left)
  {
    pcl::PointCloud<PCL_PointType>::Ptr cloud(new  pcl::PointCloud<PCL_PointType>); 
    pcl::io::loadPCDFile<PCL_PointType>(left ? out_left : out_right,*(cloud.get()));
  }

  void useSimpleTriangulation(cv::InputArray _disp, bool left, float d_range)
  {
    cv::Mat depth, disp = _disp.getMat();
    depth = cv::Mat::zeros(height,width,CV_32FC3);
    for(int y = 0; y < disp.rows; ++y)
      for(int x = 0; x < disp.cols; ++x)
	depth.at<cv::Vec3f>(y,x) = cv::Vec3f(x,y,(50*d_range)/disp.at<uchar>(y,x));
    
    FloatVector bounds = removeInfAndGetBounds(depth);
    showPCL(depth,left);
  }
  
public:
  
  Reconstruction(cv::InputArray img_left, cv::InputArray img_right, cv::InputArray _F, cv::InputArray _K, std::vector<cv::Point2f> matches, bool rectify)
  {
    rectified = rectify;
    cv::Mat K = _K.getMat();
    F = _F.getMat();
    imgL = img_left.getMat();
    imgR = img_right.getMat();
    cv::Point2f leftPt, rightPt;
    width = imgL.cols;
    height = imgL.rows;
    
    if(rectified)
    {
      F = cv::Mat::zeros(3,3,CV_64F);
      F.at<double>(2,1) = 1;
      F.at<double>(1,2) = -1;
      leftPt = matches[2];
      rightPt = matches[3];
    } else {
      leftPt = matches[0];
      rightPt = matches[1];
    }
    
    if(K.size().area() == 9)
    {
      getProjectionMatrix(K,leftPt,rightPt);
    }
    else
    {
      getProjectionMatrices();
    }
  };
  
  ~Reconstruction()
  {
  };
  
  void solve(float disprange, cv::InputArray _dispMapL, cv::InputArray _dispMapR)
  {
    std::cout << "Simple Triangulation method" << std::endl;

//     useSimpleTriangulation(_dispMapL,true, disprange);
    useSimpleTriangulation(_dispMapR,false, disprange);
    
    if(!Q.empty())
    {
      std::cout << "Reprojection matrix method" << std::endl;
      useQ(_dispMapL, true);
      useQ(_dispMapR, false);
    }
  }
};

#endif 
