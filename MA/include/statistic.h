// Copyright (C) 2015 Dominik Wetzel <dominik.wetzel@fh-zwickau.de>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef PM_STATS_H_
#define PM_STATS_H_

#include <string>
#include <fstream>
#include <sstream>
#include <opencv2/opencv.hpp>
#include <ctime>
#include "helper.h"
#include "types.h"

extern int pm_iters;
extern int rectify;
extern std::string output_ext;

class Statistic {
private:
  class HistogramBGR {
  private:
    bool used = false;
    float green_count = 0;
    
    int hist_size;
    int imgH;
    
    std::vector<float> b_hist;
    std::vector<float> r_hist;
    float color_step;
    
    std::map<int,int> seps;
    std::vector<int> keys;
    
  public:
    inline static void fitToImg(std::vector<float>& vec, int imgH, int max)
    {
      float ratio = max != 0 ? imgH / (float) max : 0.f;
      for(int i = 0; i < vec.size(); ++i)
      {
	vec[i] *= ratio;
      }
    }
    
    inline HistogramBGR(int imgH, int size) 
    : imgH(imgH), hist_size(size), b_hist(size),r_hist(size)
    {
      color_step = 200.f / size;
      for(int i = 0; i < size; ++i)
      {
	int key = 55 + (int) ((i+1)*color_step);
	seps.insert(std::pair<int,int>(key,i));
	keys.push_back(key);
      }
    }
    inline ~HistogramBGR() {}
    
    inline void addToHistogram(int b, int g, int r)
    {
      if(!used)
	used = true;
      if(b == 0 && r == 0) ++green_count;
      else if(b > 0) ++b_hist[seps[b]];
      else if(r > 0) ++r_hist[seps[r]];
    }
    
    inline double calculateMax(double max_other = 0.0)
    {
      if(!used) return 0;
      double max, max_b, max_r;
      cv::minMaxLoc(b_hist,NULL,&max_b);
      cv::minMaxLoc(r_hist,NULL,&max_r);
      max = max_b > max_r ? max_b : max_r;
      max = max_other > max ? max_other : max;
      return max;
    }
    
    inline cv::Mat getHistogram(double max_other = 0.0)
    {
      if(!used) return cv::Mat();
      
      //size for each channel
      int channel_w = 5;
      // channels * size for each channel + channel for ratio
      int imgW = channel_w*hist_size*2 + channel_w;
      
      cv::Mat histImage(imgH, imgW, CV_8UC3, cv::Scalar(255,255,255));
      
      double max = calculateMax(max_other);
      
      std::vector<float> ratio(3);
      ratio[1] = green_count;
      for(int i = 0; i < hist_size; ++i)
      {
	ratio[0] += r_hist[i];
	ratio[2] += b_hist[i];
      }
      double ratiomax;
      cv::minMaxLoc(ratio,NULL,&ratiomax);
      fitToImg(ratio,imgH,ratiomax);
      fitToImg(b_hist,imgH,max);
      fitToImg(r_hist,imgH,max);
      
      int y0 =imgH-1;
      
      if(ratiomax > 0)
      {
	float r = imgH / (ratio[0] + ratio[1] + ratio[2]);
	int x0 = 0, x1 = x0 + channel_w-1;
	int y1 = y0 - (int) (ratio[0] *r), y2 = y1 - (int) (ratio[1] *r);
	
	cv::rectangle(histImage,cv::Point(x0,y0),cv::Point(x1,y1),cv::Scalar(0,0,255),CV_FILLED);
	cv::rectangle(histImage,cv::Point(x0,y1),cv::Point(x1,y2),cv::Scalar(0,255,0),CV_FILLED);
	cv::rectangle(histImage,cv::Point(x0,y2),cv::Point(x1,0),cv::Scalar(255,0,0),CV_FILLED);
      }
      /// Draw for each channel
      for( int i = 0; i < hist_size; ++i )
      {
	cv::rectangle(histImage,cv::Point(i*2*channel_w + 1*channel_w,y0),cv::Point(i*2*channel_w + 2*channel_w,y0-b_hist[i]),cv::Scalar(keys[i],0,0),CV_FILLED);
	cv::rectangle(histImage,cv::Point(i*2*channel_w + 2*channel_w,y0),cv::Point(i*2*channel_w + 3*channel_w,y0-r_hist[i]),cv::Scalar(0,0,keys[i]),CV_FILLED);
      }
      
      //       Helper::showImage("Histogram",histImage,true);
      return histImage;
    }
    
    inline float getColorStep()
    {
      return color_step;
    }
  };
  
  std::string output_dir, statFile;
  int around;
  std::FILE* stats;
  std::map<std::string,std::vector<cv::Mat> > iterImages;
  std::map<std::string,std::vector<HistogramBGR> > bgr_histograms; 
  
  const std::string FORMAT_RECT = "%-25s | %15s | %15s | %15s | %15s | %15s | %15s | %15s";
  bool dummy = false;
  
  inline void writeOut()
  {
    std::fclose(stats);
    stats = std::fopen(statFile.c_str(), "a");
  }
  
  inline float getMedian(FloatVector& leftcosts, FloatVector& rightcosts, double& min, double& max , double& sum, FloatVector* _medianCost = NULL)
  {
    static const double INF_MAX = 99999.0f; 
    int size = leftcosts.size()*2;
    min = INFINITY;
    max = -INFINITY;
    float medianCost[size];
    
    for(int i = 0; i < size; ++i){
      float tmp;
      if(i >= leftcosts.size())
	tmp = rightcosts[i - leftcosts.size()];
      else
	tmp = leftcosts[i];
      medianCost[i] = tmp;
      if(tmp < min) min = tmp;
      if(tmp > max && tmp < INFINITY) max = tmp;
      //if(tmp == INFINITY) tmp = INF_MAX;
      //sum += tmp;
    }
    sum = 0.0;
    for(int i = 0; i < size; ++i)
    {
      float tmp = medianCost[i];
      if(tmp == INFINITY)
      {
	tmp = max;
	medianCost[i] = max;
      }
      sum += tmp;
    }
    std::sort(medianCost, medianCost + size);
    
    if(_medianCost)
    {
      _medianCost->clear();   
      _medianCost->resize(size);
      for(int i = 0; i < size; ++i)
      {
	_medianCost->operator[](i) = medianCost[i];
      }
    }
    return (medianCost[size/2] + medianCost[size/2 - 1]) / 2.0;
  }
  
public:
  inline Statistic()
  {
    dummy = true;
  }
  
  inline Statistic(std::string outputDir, std::string dispfn, int around, bool override = false, std::string prefix = "") : output_dir(outputDir), around(around)
  {
    dummy = false;
    statFile = output_dir + prefix + "stats.txt";
    stats = std::fopen(statFile.c_str(),override ? "w" : "a");
    time_t time;
    std::time(&time);
    fprintf(stats,"Stats for %s at %s\n",dispfn.c_str(), std::ctime(&time));
    writeOut();
  }
  
  inline ~Statistic()
  {
    if(dummy) return;
    writeLine("-------------------------END-------------------------");
    std::fclose(stats);
  }
  
  inline void writeRectifyHeader(cv::InputArray points, cv::InputArray out_points, std::vector<cv::Point2f> rectangle, bool manual = false)
  {
    if(dummy) return;
    std::stringstream rect;
    if(manual) {
      rect << "MANUAL";
    } else {
      rect << "ASIFT\nrectangle: " << rectangle[0] << "x" << rectangle[1];
    }
    std::fprintf(stats,"Rectification:\ninlier:%d  outlier:%d  match_method:%s\n", points.size().area(), out_points.size().area(), rect.str().c_str());
    std::fprintf(stats,(FORMAT_RECT + " | %15s\n").c_str(), "method", "avgEpiLineDist","usedInner", "ratio", "avgInErr", "usedOuter", "ratio", "avgOutErr", "time in s");
    std::fprintf(stats,"\n");
    writeOut();
  }
  
  inline void writeRectStat(std::string& prefix, float& epiError, cv::InputArray points, cv::InputArray out_points, std::tuple<float,int>& inlier, std::tuple<float,int>& outlier, double& tTime)
  {
    if(dummy) return;
    float inPts = points.size().area();
    float outPts = out_points.size().area();
    int usedOut = std::get<1>(outlier);
    int usedIn = std::get<1>(inlier);
    std::fprintf(stats,(FORMAT_RECT + " | %15f\n").c_str(), 
		 prefix.c_str(),  // method
		 std::to_string(epiError).c_str(),  // epipolarline distance
		 std::to_string(usedIn).c_str(),  // used inliers
		 std::to_string(inPts == 0 ? INFINITY : usedIn / inPts).c_str(), // inlier ratio
		 std::to_string(std::get<0>(inlier)).c_str(), // inlier error
		 std::to_string(usedOut).c_str(),  // used outliers
		 std::to_string(outPts == 0 ? INFINITY : usedOut / outPts).c_str(),  // outlier ratio
		 std::to_string(std::get<0>(outlier)).c_str(),  // outlier error
		 tTime);  // time consumed
    writeOut();
  }
  
  /**
   * Write a line with tabulator parting
   */
  inline void writeLine(std::vector<std::string>& elements)
  {
    if(dummy) return;
    std::stringstream buf;
    for(std::string elem : elements)
    {
      buf << elem << "\t";
    }
    buf << std::endl;
    std::fprintf(stats,"%s",buf.str().c_str());
    writeOut();
  }
  
  inline void writeLine(const std::string& elem) 
  {
    if(dummy) return;
    std::fprintf(stats,"%s\n",elem.c_str());
    writeOut();
  }
  
  inline void writeImage(const std::string& filename, cv::InputArray img)
  {
    if(dummy) return;
    std::string file = output_dir + filename;
    cv::imwrite(file, img);
    Helper::addTo7zFile(output_dir,file);
  }
  
  inline void writeIterImage(const std::string& filename)
  {
    if(dummy) return;
    auto vecI = iterImages[filename];
    auto vecH = bgr_histograms[filename];
    cv::Mat out = cv::Mat(0,0,vecI[0].type());
    double max = 0.0;
    
    for(int i = 0; i < vecH.size(); ++i)
    {
      max = vecH[i].calculateMax(max);
    }     
    for(int i = 0; i < vecI.size(); ++i)
    {
      cv::Mat outImg;
      Helper::makeTwoImagesOne(vecI[i],vecH[i].getHistogram(max),outImg);
//       outImg = outImg.t();
//       cv::imshow("Needed",outImg);
//       cv::waitKey(50);
      Helper::makeTwoImagesOne(out,outImg.t(),out,i > 0);
      cv::Mat sepLine = cv::Mat(out.rows,2,out.type(),cv::Scalar(255,255,255));
      Helper::makeTwoImagesOne(out,sepLine,out,i > 0);
    }
    std::string file = output_dir + filename;
    cv::imwrite(file,out.t());
    Helper::addTo7zFile(output_dir,file);
  }
  
  inline void saveNeighborCount(const std::string& filename, int iter, int view, std::vector<int>& neighborCount)
  {
    if(dummy) return;
    auto img = iterImages[filename][iter];
    int width = img.cols / 2;
    int size = neighborCount.size(); 
    for(int y = 0; y < img.rows; ++y)
      for(int x = 0; x < width; ++x)
	img.at<uchar>(y,x + view * width) = (uchar) (neighborCount[y*width + x] * (255 / MAXNEIGHBOR));
    iterImages[filename][iter] = img;
  }
  
  inline void saveNeighbor(const std::string& filename, int iter, cv::Size size, PlaneVector& left, PlaneVector& right)
  {
    int width = size.width;
    for(int y = 0; y < size.height; ++y)
      for(int x = 0; x < width; ++x)
      {
	saveNeighbor(filename,iter,x,y,(int) left[y * width + x].y_disp);
	saveNeighbor(filename,iter,x + width,y,(int) right[y * width + x].y_disp);
      }
  }
  
  inline void saveNeighbor(const std::string& filename, int iter, int x, int y, int a)
  {
    if(dummy) return;
    auto hist = bgr_histograms[filename][iter];
    int b = 0, g = 0, r = 0;
    if(a < 0) {
      b = 55 + (int) ((-1)*a*hist.getColorStep()); g = 0 ; r = 0;
    }
    if(a > 0) {
      b = 0; g = 0; r = 55 + (int) (a*hist.getColorStep());
    }
    hist.addToHistogram(b,g,r);
    bgr_histograms[filename][iter] = hist;
    iterImages[filename][iter].at<cv::Vec3b>(y,x) = cv::Vec3b(b,g,r);
  }
  
  inline void createIterImages(const std::string& filename, int iters, cv::Size size, int type)
  {
    if(dummy) return;
    auto vecI = iterImages[filename];
    auto vecH = bgr_histograms[filename];
    for(int i = 0; i < iters;++i)
    {
      vecI.push_back(cv::Mat::zeros(size,type));
      vecH.push_back(HistogramBGR(size.height,around));
    }
    iterImages[filename] = vecI;
    bgr_histograms[filename] = vecH;
  }
  
  inline void writeHistogram(FloatVector& leftcosts, FloatVector& rightcosts, unsigned sep_step = 200, unsigned pixPerPix = 0)
  {
    if(dummy) return;
    static const int HIST_WIDTH = 50;
    static const float PIXELHEIGHT = 0.001;
    static const double HIST_MAX = 2000;
    float size = leftcosts.size()*2;
    
    FloatVector medianCost;
    double max,min,sum;
    getMedian(leftcosts,rightcosts,min,max,sum,&medianCost);
    
    int maxbins = (int) ceil((HIST_MAX + 1) / sep_step);
    int bins = (int) ceil(max / sep_step);
    if(max == INFINITY || bins > maxbins) bins = maxbins;
    std::vector<float> hist(bins); 
    
    int bin = 0;
    for(int i = 0; i < size; ++i)
    {
      float val = medianCost[i];
      if(bin < bins - 1 && val > (bin + 1)*sep_step)
	++bin;
      ++hist[bin];
    }
    cv::minMaxLoc(hist,NULL,&max);
    
    std::vector<std::string> histTxt;
    histTxt.push_back("Histogram:");
    for(int i = 0; i < maxbins; ++i)
    {
      int j = 0;
      if(i <= bin)
	j = hist[i];
      histTxt.push_back(std::to_string((i+1)*sep_step) + "=" + std::to_string(j));
    }
    writeLine(histTxt);
    
// #ifdef SMTHG
    int pixHeight = pixPerPix < 1 ? (int) (PIXELHEIGHT * size) : pixPerPix;      
    int height = (int) ceil(max / pixHeight);
    cv::Mat histogram = cv::Mat::zeros(height,HIST_WIDTH*bins,CV_8UC1);
    for(int i = 0; i < bins; ++i)
    {
      cv::Mat dst;
      int x0 = HIST_WIDTH*i;
      int x1 = x0 + HIST_WIDTH;
      cv::rectangle(histogram,cv::Point(x0,height-1), cv::Point(x1,height-1-hist[i]/pixHeight),cv::Scalar(i & 1 ? 50 : 100),CV_FILLED);

      cv::putText(histogram,
		  i < maxbins -1 ? std::to_string((int) ((i+1)*sep_step)) : std::string(">" + std::to_string(HIST_MAX)),
		  cv::Point(x0,height-5),cv::FONT_HERSHEY_PLAIN,1,cv::Scalar(255));
      
      // Prepare String
      char str[256];
      sprintf(str,"%.2f %%",hist[i]*100/size);
      std::string txt(str);
      
      // put String into image (rotated)
      histogram = histogram.t();
      cv::flip(histogram,dst,1);
      cv::putText(dst,txt,cv::Point(30,x1 - HIST_WIDTH / 2),cv::FONT_HERSHEY_PLAIN,1,cv::Scalar(255));
      cv::flip(dst,histogram,1);
      histogram = histogram.t();
    }
    cv::putText(histogram,"max: " + std::to_string((int) max),cv::Point(histogram.cols - 100,10),cv::FONT_HERSHEY_PLAIN,0.7,cv::Scalar(255));
    std::string file = output_dir + "costHist" + output_ext + ".png"; 
    cv::imwrite(file,histogram);
    Helper::addTo7zFile(output_dir,file);
// #endif
  }
  
  inline void writeEnergy(int iter, FloatVector& leftcosts, FloatVector& rightcosts)
  {
    if(dummy) return;
    double min,max,sum;
    float median = getMedian(leftcosts,rightcosts,min,max,sum);
    std::vector<std::string> vec;
    vec.push_back("\nIteration " + std::to_string(iter) + ":\n");
    vec.push_back("average matchcost: " + std::to_string(sum / (2*leftcosts.size()) ));
    vec.push_back("min/max matchcost: [" + std::to_string(min) + " | " + std::to_string(max) + "]");
    vec.push_back("median matchcost: " + std::to_string(median));
    writeLine(vec);
  }
  
  inline bool isDummy()
  {
    return dummy;
  }
};

#endif
