// Copyright (C) 2015 Dominik Wetzel <dominik.wetzel@fh-zwickau.de>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef QER_H
#define QER_H

#include <opencv2/opencv.hpp>

namespace qer {
  std::pair<float,float> rectify(cv::InputArray point1, cv::InputArray point2, cv::OutputArray F, cv::Size size,cv::OutputArray Hl, cv::OutputArray Hr, cv::OutputArray K_ = cv::noArray());
}

#endif
