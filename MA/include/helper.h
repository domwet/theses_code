// Copyright (C) 2015 Dominik Wetzel <dominik.wetzel@fh-zwickau.de>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef HELPER_H
#define HELPER_H

#include <opencv2/opencv.hpp>
#include <stdlib.h>
#include <iostream>

namespace Helper {  
  inline static cv::Point2f findWhitePointInImage(cv::InputArray _img, cv::Vec3b pattern = cv::Vec3b(255,255,255), int startx = 0, int starty = 0) {
    cv::Mat img = _img.getMat();
    bool found = false;
    std::set<int> xx, yy;
    int saved_y = 0;
    for(int y = starty; y < img.rows; ++y) {
      if(found && saved_y == (y-2)) break;
      for(int x = startx; x < img.cols; ++x) {
	if(found && saved_y == y && img.at<cv::Vec3b>(y,x) != pattern) break;
	if(img.at<cv::Vec3b>(y,x) == pattern) {
	  found = true;
	  xx.insert(x);
	  yy.insert(y);
	  saved_y = y;
	}
      }
    }
    if(found) {
      float xs = 0,ys = 0;
      for(int _x : xx)
	xs += _x;
      for(int _y : yy)
	ys += _y;
      return cv::Point2f(xs / xx.size(), ys / yy.size());
    }
    return cv::Point2f(-1,-1);
  }
  
  inline void makeTwoImagesOne(cv::InputArray img1, cv::InputArray img2, cv::OutputArray out, bool check = false){
    cv::Mat left,right,two;
    left = img1.getMat().clone();
    right = img2.getMat().clone();
    out.create(right.rows < left.rows ? left.rows : right.rows,left.cols + right.cols,img1.type());
    two = out.getMat();
    size_t es = two.elemSize();
    int ch = two.channels();
    for(int y = 0; y < two.rows; ++y) {
      for(int x = 0; x < two.cols; ++x) {
	uchar* ptr = two.data + y * two.cols * es + x * es;
	uchar* lr;
	if(x < left.cols)
	  lr = left.data + y * left.cols * es + x * es;
	else
	  lr = right.data + y * right.cols * es + (x - left.cols) * es;
	for(int c = 0; c < ch; ++c)
	  *(ptr++) = *(lr++);
      }
    }
  }
  
  inline void remapPoints(cv::InputArray _pts, cv::OutputArray _outpts, cv::Size imgSize, cv::InputArray mapx, cv::InputArray mapy) {
    cv::Mat img, pts, opts;
    pts = _pts.getMat();
    std::vector<cv::Point2f> out;
    for(int i = 0; i < pts.size().area(); ++i)
    {
      cv::Point2f p = pts.at<cv::Point2f>(i);
      img = cv::Mat::zeros(imgSize,CV_8UC3);
      img.at<cv::Vec3b>((int) p.y,(int) p.x) = cv::Vec3b(255,255,255);
      cv::remap(img,img,mapx,mapy,CV_INTER_NN);
      out.push_back(findWhitePointInImage(img));
    }
    _outpts.create(out.size(),1,pts.type());

    opts = _outpts.getMat();
    for(std::size_t i = 0; i < out.size(); ++i) {
      opts.at<cv::Point2f>(i) = out[i];
    }
  }
  
  inline void printPointArray(cv::InputArray _array)
  {
    std::cout << "Points: " << std::endl;
    cv::Mat array = _array.getMat();
    for(int i = 0; i < array.size().area(); ++i)
    {
      std::cout << array.at<cv::Point2f>(i) << " ";
    }
    std::cout << std::endl << std::endl;
  }  
  
  inline void checkBounds(cv::InputOutputArray points1, cv::InputOutputArray points2, cv::Size imgSize)
  {
    float w = imgSize.width;
    float h = imgSize.height;
    cv::Mat pt1, pt2;
    pt1 = points1.getMat().clone();
    pt2 = points2.getMat().clone();
    std::vector<cv::Point2f> pts1, pts2;
    int size = pt1.size().area() < pt2.size().area() ? pt1.size().area() : pt2.size().area();
    for(int i = 0; i < size; ++i){
      cv::Point2f p1,p2;
      p1 = pt1.at<cv::Point2f>(i);
      p2 = pt2.at<cv::Point2f>(i);
      if(p1.x >= w || p2.x >= w || p1.y >= h || p2.y >= h ||
	 p1.x < 0.0 || p2.x < 0.0 || p1.y < 0.0 || p2.y < 0.0)
	continue;
      pts1.push_back(p1);
      pts2.push_back(p2);
    }
    points1.clear();
    points2.clear();
    points1.create(pts1.size(),1, pt1.type());
    points2.create(pts2.size(),1, pt2.type());
    pt1 = points1.getMat();
    pt2 = points2.getMat();
    for(std::size_t i = 0; i < pts1.size(); ++i) {
      pt1.at<cv::Point2f>(i) = pts1[i];
      pt2.at<cv::Point2f>(i) = pts2[i];
    }
  }
  
  static inline cv::Point createBorderPoint(cv::Size size, cv::Vec3f line, float x, float y)
  {
    if(x > size.width - 1) 
    {
      x = size.width -1;
      y = -(line[2]+line[0]*(size.width -1))/line[1];
    }
    if(x < 0)
    {
      x = 0;
      y = -line[2]/line[1];
    }
    return cv::Point(x,y);
  }  
  
  /**
   * computes the start and endpoint for the lines (so it can be drawn)
   * returns true if all lines are not horizontal
   */
  static inline bool computeLinePoints(cv::Size size, std::vector<cv::Vec3f> lines, std::vector<cv::Point2f> &points) {
    bool vertical = true;
    cv::Point p1,p2;
    points.clear();
    for(cv::Vec3f line : lines){
      if(line[1] != 0 && fabs(line[0]/line[1]) < 1){
	vertical = false;
	p1 = cv::Point(0,-line[2]/line[1]);
	p2 = cv::Point(size.width - 1,-(line[2]+line[0]*(size.width -1))/line[1]);
      } else {
	float x = -line[2]/line[0];
	float y = 0;
	p1 = createBorderPoint(size,line,x,y);
	x = -(line[2]+line[1]*(size.height -1))/line[0];
	y = size.height - 1;
	p2 = createBorderPoint(size,line,x,y);
      }
      points.push_back(p1);
      points.push_back(p2);
    }
    return vertical;
  }
  
  /**
  * compute the epipolar lines for given points
  */
  inline void computeEpilines(cv::Size imgSize, cv::InputArray F, std::vector<cv::Vec3f> &lines_l, std::vector<cv::Vec3f> &lines_r, std::vector<cv::Point2f> initial = std::vector<cv::Point2f>(), int img = 1, int amount = 20) {
    std::vector<cv::Point2f> pts;
    std::vector<int> xs;
    std::vector<int> ys;
    if(initial.size() < 1){
      int y_disp = imgSize.height / amount;
      int width = imgSize.width;
      int y = 0;
      for(int i = 0; i < amount; ++i) {
	y += y_disp;
	int x = rand() % width;
	ys.push_back(y);
	xs.push_back(x);
	pts.push_back(cv::Point2f(x,y));
      }
    } else {
      for(cv::Point2f p : initial) {
	ys.push_back(p.y);
	xs.push_back(p.x);
	pts.push_back(p);
      }
    }

    cv::computeCorrespondEpilines(pts,img,F,lines_r);
    pts.clear();
    for(std::size_t i = 0; i < lines_r.size(); ++i) {
      cv::Vec3f line = lines_r[i];
      float x, y;
      if(line[1] != 0 && fabs(line[0]/line[1]) < 1){
	x = xs[i];
	y = -(line[2]+line[0]*xs[i])/line[1];
      } else {
	x = -(line[2]+line[1]*ys[i])/line[0];
	y = ys[i];
      }
      pts.push_back(cv::Point2f(x,y));
    }

    cv::computeCorrespondEpilines(pts,(img & 1) + 1,F,lines_l);
  }
  
  inline void tokenize(const std::string & str, std::vector<std::string> & tokens, const std::string & delimiters = " ")
  {
    tokens.clear();
    
    std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    std::string::size_type pos     = str.find_first_of(delimiters, lastPos);
    
    while (std::string::npos != pos || std::string::npos != lastPos) {
      tokens.push_back(str.substr(lastPos, pos - lastPos));
      lastPos = str.find_first_not_of(delimiters, pos);
      pos = str.find_first_of(delimiters, lastPos);
    }
  }
  
  inline void showImage(std::string windowname, cv::InputArray img, bool create = false)
  {
    if(create) cv::namedWindow(windowname);
    cv::imshow(windowname,img);
    cv::waitKey(0);
    if(create) cv::destroyWindow(windowname);
  }
  
  inline void addTo7zFile(const std::string& output_dir,const std::string& file,const std::string& out = "data")
  {
    std::string cmd = "7z d " + output_dir + out + ".7z " + file;
    system(cmd.c_str());
    cmd = "7z a " + output_dir + out + ".7z " + file;
    system(cmd.c_str());
    cmd = "rm " + file;
    system(cmd.c_str());
  }
  
  inline void extractFrom7zFile(const std::string& output_dir,const std::string& _file,const std::string& out = "data")
  {
    std::string file = _file.substr(output_dir.length());
    std::string cmd = "7z x -y -o" + output_dir + " " + output_dir + out + ".7z " + file;
    system(cmd.c_str());
  }
  
  inline void writeMatches(std::vector<cv::Point2f> pts0, std::vector<cv::Point2f> pts1, cv::InputArray src, cv::OutputArray dst)
  {
    src.getMat().copyTo(dst);
    cv:: Mat img = dst.getMat();
    int width = img.cols / 2;
    for(int i = 0; i < pts0.size(); ++i)
    {
      cv::Point2f p1 = pts0[i], p2 = cv::Point2f(pts1[i].x + width, pts1[i].y);
      cv::line(img, p1, p2,cv::Scalar(255,255,255));
    }
  }
  
}

#endif
