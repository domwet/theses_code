// Copyright (C) 2015 Dominik Wetzel <dominik.wetzel@fh-zwickau.de>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef PM_TYPES_H
#define PM_TYPES_H

enum Refinement
{
  NONE,
  FULL,
  ROF,
  ROINP
};

struct Plane
{
  float a=0, b=0, c=0;		// Plane Params
  float z0=0;			// depth
  float nx=0, ny=0, nz=0;	// normal vector
  float y_disp = 0; 		// y_disp + 0.125 == dummy
};
typedef std::vector<Plane> PlaneVector;
typedef std::vector<float> FloatVector;
typedef unsigned int uint;

const int MAXNEIGHBOR = 5;
#endif
