// Copyright (C) 2015 Dominik Wetzel <dominik.wetzel@fh-zwickau.de>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef POLLEFEY_H
#define POLLEFEY_H
#include <opencv2/opencv.hpp>

namespace pollefey 
{
    void rectify(cv::InputArray points1, cv::InputArray points2, cv::InputArray F, cv::Size imgSize, cv::OutputArray mapx1, cv::OutputArray mapy1, cv::OutputArray mapxr, cv::OutputArray mapxl);
}

#endif
