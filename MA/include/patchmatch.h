// Copyright (C) 2015 Dominik Wetzel <dominik.wetzel@fh-zwickau.de>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef _PATCHMATCH_H_
#define _PATCHMATCH_H_

#include <algorithm>
#include <cmath>
#include <iostream>
#include <fstream>
#include <opencv2/opencv.hpp>
#include <utility>
#include <stdlib.h>
#include "omp.h"
#include "helper.h"
#include "statistic.h"
#include "types.h"
#include "open_cl_stuff.h"

extern std::string output_ext, output_dir;

class PatchMatch
{ 
private:
  Statistic* stat;
  
  size_t randNorm;
  
  PlaneVector viewneighbors;
  std::vector<int> neighborCount;
  FloatVector exps_color;
  
  PlaneVector left_planes;
  PlaneVector right_planes;
  FloatVector left_costs;
  FloatVector right_costs;
  cv::Mat left;
  cv::Mat right;
  cv::Mat smooth_left;
  cv::Mat smooth_right;
  
  PlaneVector* cur_planes = NULL;
  PlaneVector* other_planes = NULL;
  FloatVector* cur_costs = NULL;
  FloatVector* other_costs = NULL;
  cv::Mat* cur_img = NULL;
  cv::Mat* other_img = NULL;;
  cv::Mat* cur_smooth = NULL;
  
  int iterations = 3;
  int windowsize = 31;
  float alpha = 0.9f;
  
  float tau1 = 30.f;
  float tau2 = 4.f;
  float gamma_color = 10.f;
  float around_cost = 0.05f;
  
  float min_disp = 0.f;
  float max_disp = 255.0f;
  float bordercostspercentage = 1.85f;
  float lr_check_threshold = 1.f;
  int cpu_count = 8;
  Refinement refinement = Refinement::FULL;

  float bordercosts;
  float grayscale;  
  int imgW, imgH;
  int vnidx3, vnidx4, widx3, widx4;
  int prefix;
  bool solved = false;
  
  int around;
  int full_around;
  FloatVector additionalData = FloatVector(16);
   
  inline float getD(int x, int y, Plane& plane)
  {
    return plane.a * (float) x + plane.b * (float) y + plane.c;
  }
  
  inline Plane& getPlane(int x, int y, bool cur = true)
  { 
    return (cur ? cur_planes : other_planes)->operator[](y * imgW + x);
  }  
  
  inline float& getCost(int x, int y, bool cur = true)
  {
    return (cur ? cur_costs : other_costs)->operator[](y * imgW + x);
  }
  
  inline bool checkPlane(int x, int y, Plane& cur_plane, Plane& new_plane, float& bestcosts, int around = 0)
  {
    around = new_plane.y_disp;
    
    float test = matchcosts(x,y,new_plane,bestcosts,around);
    if(test <= bestcosts)
    {  
      cur_plane = new_plane;
      bestcosts = test;
      return true;
    }
    return false;
  }
  
  inline void initAdditionalData()
  {
    additionalData[0] = RAND_MAX;
    additionalData[1] = windowsize;
    additionalData[2] = min_disp;
    additionalData[3] = max_disp;
    additionalData[4] = bordercosts;
    additionalData[5] = imgW;
    additionalData[6] = imgH;
    additionalData[7] = full_around;
    additionalData[8] = vnidx3;
    additionalData[9] = vnidx4;
    additionalData[13] = alpha;
    additionalData[14] = tau1;
    additionalData[15] = tau2;
  
    randNorm = 0;
    float max_delta_z = max_disp + 1.f;
    while(max_delta_z > 0.1)
    {
      ++randNorm;
      max_delta_z /= 2.f;
    }
  }
  
  inline void setAdditionalData(size_t randCount, int change = 0,int lines = 0)
  {
    additionalData[7] = 2*lines + 1;
    additionalData[10] = prefix;
    additionalData[11] = randCount;
    additionalData[12] = change;
  }
  
  // computes a lookup table for exp for computational speed
  void precomputeexps();
  float matchcosts(int px, int py, Plane& plane, float bestcosts, int around = 0);
  std::pair<float,float> getL1Dist(int x0, int y0, float x1f, int y1, cv::Mat* curImg, cv::Mat* otherImg);
  float colorweight(cv::Vec3b& cp, int qx, int qy);
  float pixelcosts(int x0, int y0, float x1, int around);
  Plane getrandomplane(int x, int y, Plane* c_plane = NULL, float max_delta_z = 0, float max_delta_angle = 0);
  void randomInit(OCL* ocl);
  void changePointers(bool toLeft);
  void solvePatchMatch(OCL* ocl);
  void solveOneImage(int it, int even, int change, int start_x, int end_x);
  void solveOneImage(int it, int even, int change, int start_x, int end_x, int lines, int type, OCL* ocl);
  void prepareViewPropagation(int lines);
  void calculateCosts();
  
  // computes the image gradient in x-direction
  void computegradient (cv::InputArray _img, cv::OutputArray _grad);  
  void readOptions(std::map<std::string,float>& options);
  void postprocess();
  void pp_fill(std::vector<int>& invalids);
  void pp_invalidatePixels(std::vector<int>& invalids);
  void dispMap(cv::OutputArray _dispL, cv::OutputArray _dispR, bool scaled = true);
  // plots disparity map
  void writeDispMap();
  void writePlane(std::ofstream& f,Plane& p);
  void writePlanes();
  void readPlanes();
  
public:
  PatchMatch(cv::InputArray _left, cv::InputArray _right, std::map<std::string,float>& options, Statistic* stats = NULL);
  ~PatchMatch();
  bool solve(bool ocl, bool readOnly = false);
  void getDispMap(cv::OutputArray _dispL, cv::OutputArray _dispR, bool invert = false);
};

#endif
