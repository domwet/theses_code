// Copyright (C) 2015 Dominik Wetzel <dominik.wetzel@fh-zwickau.de>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

/* PatchMatch kernel */

#define AROUNDCOST 0

/*
 * constants must have their information in following order:
 * s0  rand_max
 * s1  windowsize
 * s2  min_disp
 * s3  max_disp
 * s4  bordercosts
 * s5  imgW
 * s6  imgH
 * s7  full_around
 * s8  vnidx3
 * s9  vnidx4
 * sA  prefix
 * sB  randCount
 * sC  change
 * sD  alpha
 * sE  tau1
 * sF  tau2
 */

int getBufferIndex(int x, int y, int imgW)
{
  return y * imgW + x; 
}

float getD(int2 coord, float8 plane)
{
  return plane.s0 * coord.x + plane.s1 * coord.y + plane.s2;
}

float2 getDist(__read_only image2d_t cur,
	       __read_only image2d_t other,
	       sampler_t smp,
	       int2 coord,
	       int2 coord_match,
	       float match_x,
	       int imgW,
	       float bordercosts
	      )
{
  if(coord_match.x < 0 || coord_match.x > imgW)
     return (float2)(bordercosts, bordercosts);
  float fraction = match_x - convert_float(coord_match.x);
  
  float4 curData = convert_float4(read_imageui(cur,smp,coord));
  float4 otherData = convert_float4(read_imageui(other,smp,coord_match)); 
  
  // TODO: That may be problematic
  float4 otherPlus = convert_float4(read_imageui(other,smp,(int2)(coord_match.x + 1,coord_match.y)));
   
  float4 interpol = otherData * (1.f - fraction) + otherPlus * fraction;
  float4 dif = curData - interpol;
  float4 dif_abs = convert_float4(abs(convert_int4(dif)));
  
  return (float2)((dif_abs.s0 + dif_abs.s1 + dif_abs.s2) / 3.0, dif_abs.s3);
}

float matchCosts(__read_only image2d_t cur,
		 __read_only image2d_t other,
		 __read_only image2d_t smooth,
		 sampler_t smp,
		 int2 coord,
		 float8 plane,
		 int imgW,
		 int imgH,
		 __global float* exps_color,
		 const float16 constants
		)
{
  float sum = 0.f;
  int windowsize = convert_int(constants.s1);  
  
  int start_x = coord.x - windowsize / 2;
  int end_x   = coord.x + windowsize / 2;
  int start_y = coord.y - windowsize / 2;
  int end_y   = coord.y + windowsize / 2;

  start_x = max(start_x, 0);
  end_x   = min(end_x, imgW);
  start_y = max(start_y, 0);
  end_y   = min(end_y, imgH);
  
  // insert sampler
  uint4 cp = read_imageui(smooth, smp, coord);

  for(int qy = start_y; qy < end_y; ++qy)
  {
    for(int qx = start_x; qx < end_x; ++qx)
    {
      int2 coord2 = (int2)(qx,qy);
      float d = getD(coord2,plane);
      if(d < constants.s2 - 0.5f || d > constants.s3 + 0.5f) 
	return HUGE_VALF;
      float qx1 = convert_float(qx) + constants.sA * d;
      
      // colorweight
      uint4 cq = read_imageui(smooth,smp,coord2);
      uint4 cpq = abs_diff(cp,cq);
      float colorweight = exps_color[cpq.s0 + cpq.s1 + cpq.s2];

      //  pixelcosts
      int around = convert_int(plane.s7);
      float around_cost = AROUNDCOST;
      int y_a = coord2.y + around;
      if(y_a < 0 || y_a >= imgH)
      {
	y_a = coord2.y;
	around = 0;
      }
      int2 coord_match = (int2)(convert_int(qx1),y_a);
      float2 dist = getDist(cur, other, smp,
			    coord, coord_match, qx1,imgW, constants.s4);
      float pixelcosts = (1.f - constants.sD) * min(dist.s0,constants.sE) + constants.sD * min(dist.s1,constants.sF);
      
      sum += colorweight * (pixelcosts + around_cost * pixelcosts * convert_float(abs(around)));
    }
  }  
  return sum;
}

float8 getRandomPlane(int2 coord,
		      __global float4* rands,
		      int index,
		      const float16 constants,
		      float8* curPlane,
		      float max_delta_z,
		      float max_delta_angle,
		      int i
		     )
{
  float rand_max = constants.s0;
  float rand_max_half = rand_max / 2.f;
  float disp_range = constants.s3 - constants.s2;
  float z0, nx,ny,nz;
  float a,b,c,y_disp = 0.0;
  int randCount = convert_int(constants.sB);
  float4 rand= rands[index * randCount + i];
  
  if(curPlane)
  {
    float8 p = *curPlane;
    z0 = p.s3 + (rand.s0 - rand_max_half) / rand_max_half * max_delta_z;
    nx = p.s4 + (rand.s1 - rand_max_half) / rand_max_half * max_delta_angle;
    ny = p.s5 + (rand.s2 - rand_max_half) / rand_max_half * max_delta_angle;
    nz = p.s6 + (rand.s3 - rand_max_half) / rand_max_half * max_delta_angle;
    y_disp = p.s7;
  }
  else
  {
    z0 = constants.s2 + rand.s0 / rand_max * disp_range;
    nx = (rand.s1 - rand_max_half) / constants.s5; // imgW
    ny = (rand.s2 - rand_max_half) / constants.s6; // imgH
    nz = (rand.s3 - rand_max_half) / disp_range;
  }
  
  float length = sqrt(nx * nx + ny * ny + nz * nz);
  float4 f4 = {nx,ny,nz,0.f};
  f4 /= length;
  a = -1.0 * f4.s0 / f4.s2;
  b = -1.0 * f4.s1 / f4.s2;
  c = (f4.s0 * coord.x + f4.s1 * coord.y + f4.s2 * z0) / f4.s2;
  
  return (float8)(a,b,c,z0,f4.s0,f4.s1,f4.s2,y_disp);
}

__kernel void gatherPlanes(__global float8* planes,
			   __global int* neighborCount,
			   __global float8* viewNeighbors,
			   __global float16* gathered,
			   const uint size,
			   const float16 constants,
			   int x_offset,
			   int y_offset,
			   int type
			  )
{
  int i = 0;
  int imgW = convert_int(constants.s5);
  int imgH = convert_int(constants.s6);
  int change = convert_int(constants.sC);
  int full_around = convert_int(constants.s7);
  int x = get_global_id(0);
  int y = get_global_id(1);
    
  int idx_gathered;
  if (type == 1)
    idx_gathered = y;
  else if (type == 2)
    idx_gathered = x;
  else
    idx_gathered = getBufferIndex(x,y,imgW); 
  
  idx_gathered = idx_gathered * size;
  
  y = y + y_offset;
  x = x + x_offset;
  
  int idx = getBufferIndex(x,y,imgW);
  
//   float16 dummy = (float16)(0.0);
  float8 back = (float8)((float4)(1.0,convert_float(x),convert_float(y),0.0),(float4)(0.0));
  int jMax = 1;
  
//   for(int j = 0; j <= imgW; ++j)
//     gathered[idx_gathered + i++] = (float16)(planes[getBufferIndex(j,y,imgW)],back);
  
  for(int j = 1; j <= jMax; ++j)
  {
    int ny = y - (change * j);
    if(ny >= 0 && ny < imgH)
      gathered[idx_gathered + i] = (float16)(planes[getBufferIndex(x,ny,imgW)],back);
//     else
//       gathered[idx_gathered + i] = dummy;
    ++i;
  }
  
  for(int j = 1; j <= jMax; ++j)
  {
    int nx = x - (change * j);
    if(nx >= 0 && nx < imgW)
      gathered[idx_gathered + i] = (float16)(planes[getBufferIndex(nx,y,imgW)],back);
//     else
//       gathered[idx_gathered + i] = dummy;
    ++i;
  }
  
  int count = neighborCount[idx];
  int k_xy = y*convert_int(constants.s9) + x*convert_int(constants.s8);
  for(int k = 0; k < count; ++k)
  {
    int index = k_xy + k*full_around;
    for(int j = 0; j < full_around; ++j)
    {
      float8 test = viewNeighbors[index + j];
      if(test.s7 - convert_float(convert_int(test.s7)) < 0.1)
// 	gathered[idx_gathered + i] = dummy;
//       else
	gathered[idx_gathered + i] = (float16)(test,back);
      ++i;
    }
  }
}

// Layout must be 1D
__kernel void matchCostCalc(__read_only image2d_t cur,
			    __read_only image2d_t other,
			    __read_only image2d_t smooth,
			    __global float16* gathered,
			    __global float* exps_color,
			    const float16 constants
			   )
{  
  sampler_t smp = CLK_NORMALIZED_COORDS_FALSE | //Natural coordinates
		  CLK_ADDRESS_CLAMP | //Clamp to zeros
		  CLK_FILTER_NEAREST; //Don't interpolate
		 
  int imgW = convert_int(constants.s5);
  int imgH = convert_int(constants.s6);
  int idx = get_global_id(0);
  float16 plane_info = gathered[idx];
  if(plane_info.s8)
  {
    float8 newPlane = plane_info.lo;
    float2 coord = plane_info.s9A;
    plane_info.sB = matchCosts(cur, other, smooth,
		 smp, convert_int2(coord), newPlane, imgW, imgH,
		 exps_color, constants);
    gathered[idx] = plane_info;
  }
}

__kernel void compare(__global float8* planes,
		      __global float* costs,
		      __global float16* gathered,
		      uint size,
		      const float16 constants,
		      int x_offset,
		      int y_offset,
		      int type
		     )
{
  int imgW = convert_int(constants.s5);
  int imgH = convert_int(constants.s6);
  
  int x = get_global_id(0);
  int y = get_global_id(1);
  
  int idx2;
  if (type == 1)
    idx2 = y;
  else if (type == 2)
    idx2 = x;
  else
    idx2 = getBufferIndex(x,y,imgW); 
  
  idx2 = idx2 * size;
  
  y = y + y_offset;
  x = x + x_offset;
  int idx = getBufferIndex(x,y,imgW);
  
  float bestcost = costs[idx];
  float16 best = (float16)(0.0);
  for(int i = 0; i < size; ++i)
  {
    float16 cur = gathered[idx2 + i];
    float test = cur.sB;
    if(cur.s8 && test <= bestcost)
    {
      bestcost = test;
      best = cur;
    }
  }
  if(best.s8)
  {
    costs[idx] = bestcost;
    planes[idx] = best.lo;
  }
}

__kernel void refinement(__read_only image2d_t cur,
			 __read_only image2d_t other,
			 __read_only image2d_t smooth,
			 __global float8* planes,
			 __global float* costs,
			 __global float* exps_color,
			 __global float4* rands,
			 const float16 constants,
			 float mdz,
			 float mda,
			 int i,
			 int x_offset,
			 int y_offset
			)
{
  sampler_t smp = CLK_NORMALIZED_COORDS_FALSE | //Natural coordinates
		  CLK_ADDRESS_CLAMP | //Clamp to zeros
		  CLK_FILTER_NEAREST; //Don't interpolate
  
  int imgW = convert_int(constants.s5);
  int imgH = convert_int(constants.s6);
  int x = get_global_id(0);
  int y = get_global_id(1);
  
  y = y + y_offset;
  x = x + x_offset;
  
  int2 coord = (int2)(x,y);
  
  int idx = getBufferIndex(x,y,imgW);
  
  float8 cur_plane = planes[idx];
  float bestcosts = costs[idx];
    
  float8 newPlane = getRandomPlane(coord,rands,idx,constants,
			      &cur_plane, mdz,mda,i);
  float test = matchCosts(cur, other, smooth,
			    smp, coord, newPlane, imgW, imgH,
			    exps_color, constants
			   );
  if(test < bestcosts)
  {
    costs[idx] = test;
    planes[idx] = newPlane;
  }
}

__kernel void patchmatch(__read_only image2d_t cur,
			 __read_only image2d_t other,
			 __read_only image2d_t smooth,
			 __global float8* planes,
			 __global float* costs,
			 __global float* exps_color,
			 __global float4* rands,
 			 const float16 constants,
			 __global int* neighborCount,
			 __global float8* viewNeighbors
			)
{   
  int imgW = convert_int(constants.s5);
  int imgH = convert_int(constants.s6);
  int x = get_global_id(0);
  int y = get_global_id(1);
  
  int2 coord = (int2)(x,y);
  int idx = getBufferIndex(x,y,imgW);
    
  sampler_t smp = CLK_NORMALIZED_COORDS_FALSE | //Natural coordinates
			CLK_ADDRESS_CLAMP | //Clamp to zeros
			CLK_FILTER_NEAREST; //Don't interpolate
			  
  int change = convert_int(constants.sC);
  int full_around = convert_int(constants.s7);
  
  float8 cur_plane = planes[idx];
  float bestcosts = costs[idx];
  
  float8 newPlane;
  
  // spatial lower/upper
  
  int ny = y - change;
  int nx = x - change;
  
  int2 spatialIndex = (int2)(getBufferIndex(nx,y,imgW), getBufferIndex(x,ny,imgW));
  if(ny >= 0 && ny < imgH)
  {
    newPlane = planes[spatialIndex.y];
    float test = matchCosts(cur, other, smooth,
			    smp, coord, newPlane, imgW, imgH,
			    exps_color, constants
			    );
    if(test <= bestcosts)
    {
      bestcosts = test;
      cur_plane = newPlane;
    }
  }
    
  // spatial left/right
  
  if(nx >= 0 && nx < imgH)
  {
    newPlane = planes[spatialIndex.x];
    float test = matchCosts(cur, other, smooth,
			    smp, coord, newPlane, imgW, imgH,
			    exps_color, constants
			    );
    if(test <= bestcosts)
    {
      bestcosts = test;
      cur_plane = newPlane;
    }
  }
    
  // view propagation
  int count = neighborCount[idx];
  int i_xy = y*convert_int(constants.s9) + x*convert_int(constants.s8);
  for(int i = 0; i < count; ++i)
  {
    int index = i_xy + i*full_around;
    for(int j = 0; j < full_around; ++j)
    {
      newPlane = viewNeighbors[index + j];
      if(newPlane.s7 - convert_int(newPlane.s7) > 0.1)
	continue;
      float test = matchCosts(cur, other, smooth,
			      smp, coord, newPlane, imgW, imgH,
			      exps_color, constants
			      );
      if(test <= bestcosts)
      {
	bestcosts = test;
	cur_plane = newPlane;
      }
    }
  }  
  
  // refinement
  float max_delta_z = constants.s3 + 1.f;
  float max_delta_angle = 1.f;
  int i = 0;
  while(max_delta_z > 0.1f)
  {
    newPlane = getRandomPlane(coord,rands,idx,constants,
			      &cur_plane, max_delta_z,max_delta_angle,i);
    float test = matchCosts(cur, other, smooth,
			    smp, coord, newPlane, imgW, imgH,
			    exps_color, constants
			   );
    if(test < bestcosts)
    {
      bestcosts = test;
      cur_plane = newPlane;
    }
    
    max_delta_z /= 2.f;
    max_delta_angle /= 2.f;
    ++i;
  }
  
  planes[idx] = cur_plane;
  costs[idx] = bestcosts;
}

__kernel void initialize(__read_only image2d_t curImg,
			 __read_only image2d_t otherImg,
			 __read_only image2d_t smoothImg,
			 __global float8* planes,
			 __global float* costs,
			 __global float* exps_color,
			 __global float4* rands,
			 const float16 constants
			)
{
  sampler_t smp = CLK_NORMALIZED_COORDS_FALSE | //Natural coordinates
		  CLK_ADDRESS_CLAMP | //Clamp to zeros
		  CLK_FILTER_NEAREST; //Don't interpolate
  
  int imgW = convert_int(constants.s5);
  int imgH = convert_int(constants.s6);
  int2 coord = (int2)(get_global_id(0), get_global_id(1));
  int idx = getBufferIndex(coord.x,coord.y,imgW);
  
  float8 plane = getRandomPlane(coord, rands,idx, constants,0,0,0,0);
  float cost = matchCosts(curImg,otherImg,smoothImg,smp,
			  coord,plane,imgW,imgH,
			  exps_color,constants);
 
  planes[idx] = plane;
  costs[idx] = cost;
}
